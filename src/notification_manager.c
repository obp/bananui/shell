/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-notification-manager"

#include "notification_manager.h"
#include "notification.h"
#include "shell.h"

typedef struct _BshNotificationManager
{
  BshNotifyDBusNotificationsSkeleton parent;

  int dbus_name_id;
  unsigned int last_id;
  GHashTable *notifications;
} BshNotificationManager;

static void notify_iface_init (BshNotifyDBusNotificationsIface *iface);
G_DEFINE_TYPE_WITH_CODE (BshNotificationManager,
                         bsh_notification_manager,
                         BSH_NOTIFY_DBUS_TYPE_NOTIFICATIONS_SKELETON,
                         G_IMPLEMENT_INTERFACE (
                           BSH_NOTIFY_DBUS_TYPE_NOTIFICATIONS,
                           notify_iface_init));


static gboolean
handle_close_notification (BshNotifyDBusNotifications *skeleton,
                           GDBusMethodInvocation      *invocation,
                           unsigned int                id)
{
  BshNotificationManager *self = BSH_NOTIFICATION_MANAGER(skeleton);

  g_hash_table_remove (self->notifications, GUINT_TO_POINTER(id));

  bsh_notify_dbus_notifications_complete_close_notification (skeleton, invocation);

  return TRUE;
}


static gboolean
handle_get_capabilities (BshNotifyDBusNotifications *skeleton,
                         GDBusMethodInvocation      *invocation)
{
  return FALSE;
}


static gboolean
handle_get_server_information (BshNotifyDBusNotifications *skeleton,
                               GDBusMethodInvocation      *invocation)
{
  bsh_notify_dbus_notifications_complete_get_server_information (
    skeleton, invocation, "Bananui Shell Notification Manager", "Bananui",
    BSH_VERSION, "1.2");
  return TRUE;
}


static const char *
find_event (const char *category)
{
  if (strcmp (category, "email.arrived") == 0)
    return "message-new-email";
  else if (strcmp (category, "im.received") == 0)
    return "message-new-instant";
  else if (strcmp (category, "x-bananui.alarm") == 0)
    return "alarm-clock-elapsed";

  return NULL;
}


static gboolean
handle_notify (BshNotifyDBusNotifications *skeleton,
               GDBusMethodInvocation      *invocation,
               const char                 *app_name,
               unsigned int                replaces_id,
               const char                 *app_icon,
               const char                 *summary,
               const char                 *body,
               const char *const          *actions,
               GVariant                   *hints,
               int                         expire_timeout)
{
  BshNotificationManager *self = BSH_NOTIFICATION_MANAGER(skeleton);
  BshShell               *shell = bsh_shell_get_default ();
  BshNotification        *notification = NULL;
  unsigned int            id;

  if (replaces_id) {
    id = replaces_id;
    notification = g_hash_table_lookup (self->notifications, GUINT_TO_POINTER (id));
  }

  if (!notification) {
    GVariant *item;
    GVariantIter iter;
    const char *event = NULL;
    gboolean urgent = FALSE;

    g_variant_iter_init (&iter, hints);
    while ((item = g_variant_iter_next_value (&iter))) {
      g_autofree char *key = NULL;
      g_autoptr(GVariant) value = NULL;

      g_variant_get (item, "{sv}", &key, &value);

      if (strcmp (key, "urgency") == 0) {
        if (g_variant_is_of_type (value, G_VARIANT_TYPE_BYTE) &&
            (g_variant_get_byte (value) == 2)) {
          urgent = TRUE;
        }
      } else if (strcmp (key, "category") == 0) {
        if (g_variant_is_of_type (value, G_VARIANT_TYPE_STRING)) {
          event = find_event (g_variant_get_string (value, NULL));
        }
      }

      g_variant_unref (item);
    }

    id = ++self->last_id;
    notification = bsh_notification_new (event ?: "message-missed-notification",
                                         urgent);
    g_hash_table_insert (self->notifications, GUINT_TO_POINTER(id), notification);
    bsh_shell_wake (shell);
  }

  bsh_notification_set (notification, summary, body, app_icon);

  bsh_notify_dbus_notifications_complete_notify (skeleton, invocation, id);

  return TRUE;
}


static void
notify_iface_init (BshNotifyDBusNotificationsIface *iface)
{
  iface->handle_close_notification = handle_close_notification;
  iface->handle_get_capabilities = handle_get_capabilities;
  iface->handle_get_server_information = handle_get_server_information;
  iface->handle_notify = handle_notify;
}


static void
on_bus_acquired (GDBusConnection *connection,
                 const char      *name,
                 gpointer         user_data)
{
  BshNotificationManager *self = user_data;

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self),
                                    connection,
                                    "/org/freedesktop/Notifications",
                                    NULL);
}


static void
on_name_acquired (GDBusConnection *connection,
                  const char      *name,
                  gpointer         user_data)
{
  g_debug ("Acquired name %s", name);
}


static void
on_name_lost (GDBusConnection *connection,
              const char      *name,
              gpointer         user_data)
{
  g_debug ("Lost or failed to acquire name %s", name);
}


static void
bsh_notification_manager_constructed (GObject *object)
{
  BshNotificationManager *self = BSH_NOTIFICATION_MANAGER (object);

  G_OBJECT_CLASS (bsh_notification_manager_parent_class)->constructed (object);

  self->dbus_name_id = g_bus_own_name (G_BUS_TYPE_SESSION,
                                       "org.freedesktop.Notifications",
                                       G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                                       G_BUS_NAME_OWNER_FLAGS_REPLACE,
                                       on_bus_acquired,
                                       on_name_acquired,
                                       on_name_lost,
                                       self,
                                       NULL);
}


static void
bsh_notification_manager_class_init (BshNotificationManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bsh_notification_manager_constructed;
}


static void
bsh_notification_manager_init (BshNotificationManager *self)
{
  self->notifications = g_hash_table_new_full (g_direct_hash,
                                               g_direct_equal,
                                               NULL,
                                               g_object_unref);
}


BshNotificationManager *
bsh_notification_manager_new (void)
{
  return g_object_new (BSH_TYPE_NOTIFICATION_MANAGER, NULL);
}

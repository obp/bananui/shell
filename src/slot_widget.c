/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-slot-widget"

#include "slot_widget.h"

#include <bananui/bananui.h>
#include "cellular_manager.h"
#include "enums.h"

enum {
  BSH_SLOT_WIDGET_PROP_0,
  BSH_SLOT_WIDGET_PROP_SLOT,
  BSH_SLOT_WIDGET_PROP_FLAGS,
  BSH_SLOT_WIDGET_PROP_LAST_PROP,
};
static GParamSpec *props[BSH_SLOT_WIDGET_PROP_LAST_PROP];

enum {
  SIGNAL_CLICKED,
  SIGNAL_UPDATED,
  N_SIGNALS,
};
static unsigned int signals[N_SIGNALS];

struct _BshSlotWidget {
  GObject parent;

  WroomdDBusWroomdSlot *slot;
  BshSlotWidgetFlags    flags;
  bWindow              *wnd;
  bBox                 *widget;
  /* following pointers are invalid if widget is NULL */
  bBox                 *boxes[4];
  bContent             *label;
  bContent             *error;
  bContent             *pin_prompt;
  bBox                 *pin_input;
  unsigned int          pin_length;
  char                  pin[9];
};

G_DEFINE_TYPE (BshSlotWidget, bsh_slot_widget, G_TYPE_OBJECT)


static char *
get_slot_label (WroomdDBusWroomdSlot *slot)
{
  BshCellularManager *cm         = bsh_cellular_manager_get_default ();
  const char         *status_str = NULL;

  if (wroomd_dbus_wroomd_slot_get_card_present (slot)) {
    if (wroomd_dbus_wroomd_slot_get_priority (slot) != 0) {
      const char *lock = wroomd_dbus_wroomd_slot_get_lock (slot);
      if (0 == strcmp (lock, "none")) {
        const char *state = wroomd_dbus_wroomd_slot_get_network_state (slot);
        if (0 == strcmp(state, "registered")) {
          status_str = wroomd_dbus_wroomd_slot_get_network_operator (slot);
        } else {
          status_str = state;
        }
      } else {
        status_str = "locked";
      }
    } else {
      status_str = "disabled";
    }
  } else {
    status_str = "not inserted";
  }

  return g_strdup_printf ("%s: %s",
                          bsh_cellular_manager_get_slot_name (cm, slot),
                          status_str);
}


static char *
get_pin_text (WroomdDBusWroomdSlot *slot)
{
  const char   *lock, *message;
  char         *code_name = NULL;
  gboolean      verified = FALSE;
  unsigned int  *retries = NULL, pin_retries = 0, puk_retries = 0;
  GVariant     *codes;
  GVariantIter  iter;

  if (!wroomd_dbus_wroomd_slot_get_card_present (slot))
    return NULL;
  if (wroomd_dbus_wroomd_slot_get_priority (slot) == 0)
    return NULL;
  
  lock = wroomd_dbus_wroomd_slot_get_lock (slot);
  if (0 == strcmp (lock, "none"))
    return NULL;
  if (0 == strcmp (lock, "pin")) {
    retries = &pin_retries;
    message = "PIN required";
  } else if (0 == strcmp (lock, "puk")) {
    retries = &puk_retries;
    message = "PUK required";
  } else {
    return g_strdup ("Blocked");
  }

  codes = wroomd_dbus_wroomd_slot_get_codes (slot);
  g_variant_iter_init (&iter, codes);
  g_variant_iter_next (&iter,
                       "(s(buu))",
                       &code_name,
                       &verified,
                       &pin_retries,
                       &puk_retries);

  g_debug ("%s for %s, %u retries", message, code_name, *retries);
  g_free (code_name);
  if (verified)
    g_warning ("PIN is verified but still locked");

  return g_strdup_printf ("%s (%u retries)", message, *retries);
}


static void
update_pin_input (BshSlotWidget *self)
{
  unsigned int i;

  bDestroyBoxChildren (self->pin_input);
  for (i = 0; i < self->pin_length; i++) {
    bAddBox (self->pin_input,
             bBuildBox (bDefaultTheme (NULL), "shell slot pin digit", 0, NULL));
  }
  g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
}


static int
pin_input_key_cb (void *param, void *data)
{
  bKeyEvent *ev = param;
  BshSlotWidget *self = data;
  char digit = '\0';

  switch (ev->sym) {
  case XKB_KEY_0: digit = '0'; break;
  case XKB_KEY_1: digit = '1'; break;
  case XKB_KEY_2: digit = '2'; break;
  case XKB_KEY_3: digit = '3'; break;
  case XKB_KEY_4: digit = '4'; break;
  case XKB_KEY_5: digit = '5'; break;
  case XKB_KEY_6: digit = '6'; break;
  case XKB_KEY_7: digit = '7'; break;
  case XKB_KEY_8: digit = '8'; break;
  case XKB_KEY_9: digit = '9'; break;
  case BANANUI_KEY_Back:
    if (self->pin_input && self->pin_length > 0) {
      self->pin[--self->pin_length] = '\0';
      update_pin_input (self);
      return 0;
    }
  }

  if (self->pin_input && digit && self->pin_length < 8) {
    self->pin[self->pin_length++] = digit;
    update_pin_input (self);
  }

  return 1;
}


static void
bsh_slot_widget_dispose (GObject *object)
{
  BshSlotWidget *self = BSH_SLOT_WIDGET (object);

  if (self->wnd)
    bUnregisterEventHandler (&self->wnd->keydown, pin_input_key_cb, self);

  G_OBJECT_CLASS (bsh_slot_widget_parent_class)->dispose (object);
}


static void
bsh_slot_widget_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  BshSlotWidget *self = BSH_SLOT_WIDGET (object);

  switch (property_id) {
  case BSH_SLOT_WIDGET_PROP_SLOT:
    g_value_set_object (value, self->slot);
    break;
  case BSH_SLOT_WIDGET_PROP_FLAGS:
    g_value_set_flags (value, self->flags);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
setup_pin_input (BshSlotWidget *self)
{
  if (0 == strcmp (wroomd_dbus_wroomd_slot_get_lock (self->slot), "pin")) {
    if (self->pin_input)
      return;
    bAddBox (self->boxes[3], self->pin_input = bCreateBox ("shell slot pin input"));
  } else if (self->pin_input) {
    self->pin_input = NULL;
    bDestroyBoxChildren (self->boxes[3]);
  }
}


static void
properties_changed_cb (WroomdDBusWroomdSlot *slot,
                       GVariant             *changed,
                       GStrv                 invalidated,
                       BshSlotWidget        *self)
{
  g_debug ("Slot properties changed");

  if (!self->widget)
    return;

  if (self->label) {
    g_autofree char *label = get_slot_label (self->slot);
    bSetText (self->label, label);
  }

  if (self->pin_prompt) {
    g_autofree char *text = get_pin_text (self->slot);
    if (text)
      bSetText (self->pin_prompt, text);
    else
      bClearContent (self->pin_prompt);
  }

  if (self->boxes[3])
    setup_pin_input (self);

  g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
}


static void
bsh_slot_widget_set_property (GObject *object,
                              guint property_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  BshSlotWidget *self = BSH_SLOT_WIDGET (object);

  switch (property_id) {
  case BSH_SLOT_WIDGET_PROP_SLOT:
    g_set_object (&self->slot, g_value_get_object (value));
    g_signal_connect_object (self->slot,
                             "g-properties-changed",
                             G_CALLBACK(properties_changed_cb),
                             self,
                             G_CONNECT_DEFAULT);
    break;
  case BSH_SLOT_WIDGET_PROP_FLAGS:
    self->flags = g_value_get_flags (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_slot_widget_class_init (BshSlotWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bsh_slot_widget_dispose;
  object_class->get_property = bsh_slot_widget_get_property;
  object_class->set_property = bsh_slot_widget_set_property;

  props[BSH_SLOT_WIDGET_PROP_SLOT] =
    g_param_spec_object ("slot",
                         "Slot",
                         "The SIM slot which this widget represents",
                         WROOMD_DBUS_TYPE_WROOMD_SLOT,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  props[BSH_SLOT_WIDGET_PROP_FLAGS] =
    g_param_spec_flags ("flags",
                        "Flags",
                        "Flags to enable interactive features of the widget",
                        BSH_TYPE_SLOT_WIDGET_FLAGS,
                        BSH_SLOT_WIDGET_FLAG_NONE,
                        G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  signals[SIGNAL_CLICKED] =
    g_signal_new ("clicked",
                  BSH_TYPE_SLOT_WIDGET,
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  0);

  signals[SIGNAL_UPDATED] =
    g_signal_new ("updated",
                  BSH_TYPE_SLOT_WIDGET,
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  0);

  g_object_class_install_properties (object_class, BSH_SLOT_WIDGET_PROP_LAST_PROP, props);
}


static void
bsh_slot_widget_init (BshSlotWidget *self)
{
}


BshSlotWidget *
bsh_slot_widget_new (WroomdDBusWroomdSlot *slot,
                     BshSlotWidgetFlags flags)
{
  return g_object_new (BSH_TYPE_SLOT_WIDGET,
                       "slot", slot,
                       "flags", flags,
                       NULL);
}


static int
box_destroy_cb (void *param, void *data)
{
  BshSlotWidget *widget = BSH_SLOT_WIDGET(data);
  widget->wnd = NULL;
  widget->widget = NULL;
  g_object_unref (widget);
  return 1;
}


static gboolean
emit_click (gpointer data)
{
  g_signal_emit (G_OBJECT(data), signals[SIGNAL_CLICKED], 0);
  return G_SOURCE_REMOVE;
}

static int
box_click_cb (void *param, void *data)
{
  BshSlotWidget *self = data;
  g_idle_add (emit_click, self);
  return 1;
}


static void
slot_unlocked_cb (WroomdDBusWroomdSlot *slot,
                  GAsyncResult         *res,
                  BshSlotWidget        *self)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_slot_call_unlock_finish (slot, res, &error)) {
    g_warning ("Error unlocking slot: %s", error->message);
    if (self->error) {
      bSetText (self->error, "Failed to unlock!");
      g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
    }
  } else {
    g_debug ("Successfully unlocked slot");
  }
}


static void
slot_activated_cb (WroomdDBusWroomdSlot *slot,
                   GAsyncResult         *res,
                   BshSlotWidget        *self)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_slot_call_activate_finish (slot, res, &error)) {
    g_warning ("Error activating slot: %s", error->message);
    if (self->error) {
      bSetText (self->error, "Failed to activate!");
      g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
    }
  } else {
    g_debug ("Successfully activated slot");
  }
}


static void
slot_deactivated_cb (WroomdDBusWroomdSlot *slot,
                     GAsyncResult         *res,
                     BshSlotWidget        *self)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_slot_call_deactivate_finish (slot, res, &error)) {
    g_warning ("Error deactivating slot: %s", error->message);
    if (self->error) {
      bSetText (self->error, "Failed to deactivate!");
      g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
    }
  } else {
    g_debug ("Successfully deactivated slot");
  }
}


static int
box_skright_cb (void *param, void *data)
{
  BshSlotWidget *self = data;

  if (!wroomd_dbus_wroomd_slot_get_card_present (self->slot))
    return 1;

  if (self->error) {
    bClearContent (self->error);
    g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
  }

  if (self->pin_input && self->pin_length >= 4) {
    char         *code_name = NULL;
    gboolean      verified = FALSE;
    unsigned int  pin_retries = 0, puk_retries = 0;
    GVariant     *codes;
    GVariantIter  iter;

    codes = wroomd_dbus_wroomd_slot_get_codes (self->slot);
    g_variant_iter_init (&iter, codes);
    g_variant_iter_next (&iter,
                         "(s(buu))",
                         &code_name,
                         &verified,
                         &pin_retries,
                         &puk_retries);
    if (verified)
      g_warning ("PIN is verified but still locked");
    wroomd_dbus_wroomd_slot_call_unlock (
      self->slot,
      code_name,
      self->pin,
      NULL,
      (GAsyncReadyCallback) slot_unlocked_cb,
      self);
    g_free (code_name);
  } else if (wroomd_dbus_wroomd_slot_get_priority (self->slot) == 0) {
    wroomd_dbus_wroomd_slot_call_activate (
      self->slot,
      NULL,
      (GAsyncReadyCallback) slot_activated_cb,
      self);
  } else {
    wroomd_dbus_wroomd_slot_call_deactivate (
      self->slot,
      NULL,
      (GAsyncReadyCallback) slot_deactivated_cb,
      self);
  }

  return 1;
}


static int
box_focus_cb (void *param, void *data)
{
  bFocusEvent *ev = param;
  BshSlotWidget *self = data;
  bBindStyle (self->widget, "shell slot button :focus");
  self->wnd = ev->wnd;
  bRegisterEventHandler (&self->wnd->keydown, pin_input_key_cb, self);
  g_signal_emit (G_OBJECT(self), signals[SIGNAL_UPDATED], 0);
  return 1;
}


static int
box_unfocus_cb (void *param, void *data)
{
  bFocusEvent *ev = param;
  BshSlotWidget *self = data;
  if (ev->wnd != self->wnd)
    return 1;
  bUnregisterEventHandler (&self->wnd->keydown, pin_input_key_cb, self);
  self->wnd = NULL;
  bBindStyle (self->widget, "shell slot button");
  return 1;
}


bBox *
bsh_slot_widget_into_box (BshSlotWidget *self)
{
  g_return_val_if_fail (self->widget == NULL, NULL);

  self->widget = bBuildBox (
    bDefaultTheme (NULL),
    (self->flags & BSH_SLOT_WIDGET_FLAG_IS_BUTTON) ? "shell slot button" : "shell slot",
    4,
    self->boxes);
  if (self->boxes[0]) {
    g_autofree char *label = get_slot_label (self->slot);
    bAddContent (self->boxes[0], self->label = bCreateText (label));
  } else {
    self->label = NULL;
  }
  if (self->boxes[1])
    bAddContent (self->boxes[1], self->error = bCreateEmpty ());
  else
    self->error = NULL;
  if (self->boxes[2]) {
    g_autofree char *text = get_pin_text (self->slot);
    bAddContent (self->boxes[2],
                 self->pin_prompt = text ? bCreateText (text) : bCreateEmpty());
  } else {
    self->error = NULL;
  }

  if (self->boxes[3])
    setup_pin_input (self);

  if (self->flags & BSH_SLOT_WIDGET_FLAG_IS_BUTTON) {
    self->widget->focusable = 1;
    bRegisterEventHandler (&self->widget->click, box_click_cb, self);
    bRegisterEventHandler (&self->widget->skright, box_skright_cb, self);
    bRegisterEventHandler (&self->widget->focusin, box_focus_cb, self);
    bRegisterEventHandler (&self->widget->focusout, box_unfocus_cb, self);
  }

  bRegisterEventHandler (&self->widget->destroy, box_destroy_cb, self);

  return self->widget;
}


const char *
bsh_slot_widget_get_action (BshSlotWidget *self)
{
  if (!self->wnd)
    return NULL;

  if (!wroomd_dbus_wroomd_slot_get_card_present (self->slot))
    return "";

  if (wroomd_dbus_wroomd_slot_get_priority (self->slot) != 0) {
    if (self->pin_input && self->pin_length >= 4) {
      return "Unlock";
    } else {
      return "Disable";
    }
  }
  return "Enable";
}


gboolean
bsh_slot_widget_is_unlocked (BshSlotWidget *self)
{
  return 0 == strcmp (wroomd_dbus_wroomd_slot_get_lock (self->slot), "none");
}


gboolean
bsh_slot_widget_is_selectable (BshSlotWidget *self)
{
  if (!wroomd_dbus_wroomd_slot_get_card_present (self->slot))
    return FALSE;

  if (wroomd_dbus_wroomd_slot_get_priority (self->slot) == 0)
    return FALSE;

  return bsh_slot_widget_is_unlocked (self);
}


WroomdDBusWroomdSlot *
bsh_slot_widget_get_slot (BshSlotWidget *self)
{
  return self->slot;
}


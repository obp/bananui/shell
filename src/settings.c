/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-settings"
#include <stdbool.h>
#include <bananui/bananui.h>
#include "shell.h"
#include "layerwindow.h"
#include "homescreen.h"
#include "settings.h"
#include "settings/brightness.h"

struct bsh_settings {
	struct wl_output *output;
	bWindow *wnd;
	bCard *card;
	bBox *row1;
	bool open;
	struct bsh_setting_brightness brightness;
};

static int settings_render(void *param, void *data)
{
	struct bsh_settings *s = data;
	bRenderCard(s->wnd, s->card);
	if(!s->open && bAnimationComplete(s->card)){
		settings_hide_now(s);
	}
	return 1;
}

static int settings_handle_key(void *param, void *data)
{
	struct bsh_settings *s = data;
	bKeyEvent *ev = param;
	if(ev->sym == BANANUI_KEY_Back || ev->sym == BANANUI_KEY_EndCall ||
			ev->sym == XKB_KEY_Down){
		settings_hide(s);
	}
	else if(s->card->focus == s->brightness.box && ev->sym == XKB_KEY_Left){
		brightness_decrease(&s->brightness);
		bRequestFrame(s->wnd);
	}
	else if(s->card->focus == s->brightness.box && ev->sym == XKB_KEY_Right){
		brightness_increase(&s->brightness);
		bRequestFrame(s->wnd);
	}
	return 1;
}

void settings_show(struct bsh_settings *s)
{
	if(s->open) return;
	g_debug("Opening settings");
	if(!s->wnd){
		s->wnd = bCreateLayerWindow("bananui-shell settings",
				s->output, 0, 0, ZWLR_LAYER_SHELL_V1_LAYER_TOP,
				-30, 0, 0, 0,
				ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
				ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
				ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
				ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
				0, 1);
		if(!s->wnd){
			g_critical("Failed to open settings window");
			return;
		}

		bRegisterEventHandler(&s->wnd->keydown, settings_handle_key, s);
		bRegisterEventHandler(&s->wnd->redraw, settings_render, s);

		bFocusCard(s->wnd, s->card);

		bRequestFrame(s->wnd);
	}
	bFadeCard(s->card, 0, 0);
	bFadeCard(s->card, 1, 0.2);
	bTranslateCard(s->card, 0, -s->wnd->height, 0);
	bTranslateCard(s->card, 0, 0, 0.2);
	bFocusBox(s->wnd, s->card, s->brightness.box);
	s->open = true;
}

void settings_hide_now(struct bsh_settings *s)
{
	bWindow *wnd = s->wnd;
	if(!s->wnd) return;
	s->open = false;
	s->wnd = NULL;
	g_debug("Closed settings");
	bDestroyWindow(wnd); /* does not return if inside event handler */
}

void settings_hide(struct bsh_settings *s)
{
	if(!s->open) return;
	g_debug("Closing settings");
	s->open = false;
	bFadeCard(s->card, 0, 0.2);
	bTranslateCard(s->card, 0, -s->wnd->height, 0.2);
	bRequestFrame(s->wnd);
}

struct bsh_settings *create_settings(struct wl_output *output)
{
	bBox *close_box, *close_icon_box;
	struct bsh_settings *s = g_malloc0(sizeof(struct bsh_settings));
	s->output = output;
	s->card = bCreateCard("shell settings");

	bAddBox(s->card->box, s->row1 = bCreateBox("shell settings row"));

	brightness_add(&s->wnd, s->row1, &s->brightness);

	bAddBox(s->card->box, close_box = bCreateBox("shell settings row"));
	bAddBox(close_box, close_icon_box = bCreateBox("shell settings close"));
	bAddContent(close_icon_box, bCreateIcon("go-down-symbolic", 32));
	return s;
}

void destroy_settings(struct bsh_settings *s)
{
	settings_hide_now(s);
	bDestroyCard(s->card);
	g_free(s);
}

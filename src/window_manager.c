/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-window-manager"

#include <stdlib.h>
#include <gio/gdesktopappinfo.h>
#include "bsh_wayland.h"
#include "shell.h"
#include "util.h"
#include "window_manager.h"
#include "homescreen.h"

struct wm_toplevel {
	struct bsh_window_manager *wm;
	struct zwlr_foreign_toplevel_handle_v1 *handle;
	char *app_id;
	GList *link;
	unsigned int activated : 1;
};

struct bsh_window_manager {
	GList *toplevels;
};

static void handle_toplevel_title(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle,
		const char *title)
{
	g_debug("Toplevel title: %s", title);
}

static void handle_toplevel_app_id(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle,
		const char *app_id)
{
	struct wm_toplevel *toplevel = data;
	g_free(toplevel->app_id);
	toplevel->app_id = g_strdup(app_id);
}

static void handle_toplevel_output_enter(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle,
		struct wl_output *output)
{
}

static void handle_toplevel_output_leave(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle,
		struct wl_output *output)
{
}

static void handle_toplevel_state(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle,
		struct wl_array *states)
{
	struct wm_toplevel *toplevel = data;
	struct bsh_window_manager *wm = toplevel->wm;
	enum zwlr_foreign_toplevel_handle_v1_state *state;
	unsigned int activated = 0;
	wl_array_for_each(state, states) {
		if(*state == ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_ACTIVATED){
			activated = 1;
		}
	}
	if(activated && !toplevel->activated){
		g_debug("Toplevel now activated");
		wm->toplevels = g_list_remove_link(wm->toplevels,
				toplevel->link);
		wm->toplevels = g_list_concat(toplevel->link,
				wm->toplevels);
	}
	toplevel->activated = activated;
}

static void handle_toplevel_done(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle)
{
}

static void handle_toplevel_closed(void *data,
		struct zwlr_foreign_toplevel_handle_v1 *handle)
{
	BshShell *shell = bsh_shell_get_default();
	BshWayland *wl = bsh_wayland_get_default();
	struct wm_toplevel *toplevel = data;
	struct bsh_window_manager *wm = toplevel->wm;

	wm->toplevels = g_list_delete_link(wm->toplevels, toplevel->link);
	zwlr_foreign_toplevel_handle_v1_destroy(handle);
	if(toplevel->activated){
		GList *tl;
		g_debug("Active toplevel closed (app_id %s)", toplevel->app_id);
		for(tl = wm->toplevels; tl; tl = tl->next){
			struct wm_toplevel *next = tl->data;
			if(0 == strcmp(toplevel->app_id, next->app_id)){
				g_debug("Activating toplevel with same app_id");
				zwlr_foreign_toplevel_handle_v1_activate(
						next->handle,
						bsh_wayland_get_wl_seat(wl));
				break;
			}
		}
		if(!tl){
			g_debug("Could not find toplevel with same app_id");
			homescreen_show(shell->homescreen);
		}
	}
	g_free(toplevel->app_id);
	g_free(toplevel);
}

struct zwlr_foreign_toplevel_handle_v1_listener toplevel_listener = {
	.title = handle_toplevel_title,
	.app_id = handle_toplevel_app_id,
	.output_enter = handle_toplevel_output_enter,
	.output_leave = handle_toplevel_output_leave,
	.state = handle_toplevel_state,
	.done = handle_toplevel_done,
	.closed = handle_toplevel_closed,
};

static void handle_toplevel(void *data,
		struct zwlr_foreign_toplevel_manager_v1 *manager,
		struct zwlr_foreign_toplevel_handle_v1 *handle)
{
	struct bsh_window_manager *wm = data;
	struct wm_toplevel *toplevel = g_malloc0(sizeof(struct wm_toplevel));
	g_debug("Got toplevel");
	toplevel->wm = wm;
	toplevel->handle = handle;
	wm->toplevels = toplevel->link =
		g_list_prepend(wm->toplevels, toplevel);
	zwlr_foreign_toplevel_handle_v1_add_listener(handle,
			&toplevel_listener, toplevel);
}

static const struct zwlr_foreign_toplevel_manager_v1_listener
foreign_toplevel_manager_listener = {
	.toplevel = handle_toplevel,
};

int window_manager_open_app(struct bsh_window_manager *wm,
		GAppInfo *app)
{
	BshWayland *wl = bsh_wayland_get_default();
	GList *tl;
	for(tl = wm->toplevels; tl; tl = tl->next){
		struct wm_toplevel *toplevel = tl->data;
		GDesktopAppInfo *toplevel_app =
			bsh_get_desktop_app_info_for_app_id(toplevel->app_id);

		if(!toplevel_app) continue;
		if(!g_app_info_equal(G_APP_INFO(toplevel_app), app)) continue;

		g_debug("Found matching window (app_id %s)",
				toplevel->app_id);
		zwlr_foreign_toplevel_handle_v1_activate(toplevel->handle,
				bsh_wayland_get_wl_seat(wl));
		return 1;
	}
	return 0;
}

void window_manager_go_prev(struct bsh_window_manager *wm)
{
	BshWayland *wl = bsh_wayland_get_default();
	struct wm_toplevel *toplevel;
	GList *tl;

	for (tl = wm->toplevels; tl; tl = tl->next) {
		if (!tl->next) {
			toplevel = tl->data;
			zwlr_foreign_toplevel_handle_v1_activate(
				toplevel->handle,
				bsh_wayland_get_wl_seat(wl));
			wm->toplevels = g_list_remove_link(
				wm->toplevels,
				toplevel->link);
			wm->toplevels = g_list_concat(
				toplevel->link,
				wm->toplevels);
			break;
		}
	}
}

void window_manager_go_next(struct bsh_window_manager *wm)
{
	BshWayland *wl = bsh_wayland_get_default();
	struct wm_toplevel *toplevel;

	if (!wm->toplevels || !wm->toplevels->next)
		return;

	toplevel = wm->toplevels->data;
	wm->toplevels = g_list_remove_link(
		wm->toplevels,
		toplevel->link);
	wm->toplevels = g_list_reverse(wm->toplevels);
	wm->toplevels = g_list_concat(
		toplevel->link,
		wm->toplevels);
	wm->toplevels = g_list_reverse(wm->toplevels);

	toplevel = wm->toplevels->data;
	zwlr_foreign_toplevel_handle_v1_activate(
		toplevel->handle,
		bsh_wayland_get_wl_seat(wl));
}

void window_manager_close(struct bsh_window_manager *wm)
{
	struct wm_toplevel *toplevel;

	if (!wm->toplevels)
		return;

	toplevel = wm->toplevels->data;
	zwlr_foreign_toplevel_handle_v1_close(toplevel->handle);
}

gboolean window_manager_has_window(struct bsh_window_manager *wm)
{
	if (!wm->toplevels)
		return FALSE;

	return TRUE;
}

struct bsh_window_manager *create_window_manager(void)
{
	BshWayland *wl = bsh_wayland_get_default();
	struct bsh_window_manager *wm = g_malloc0(sizeof(struct bsh_window_manager));

	zwlr_foreign_toplevel_manager_v1_add_listener(
			bsh_wayland_get_zwlr_foreign_toplevel_manager_v1(wl),
			&foreign_toplevel_manager_listener, wm);

	return wm;
}

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-homescreen"

#include <stdbool.h>
#include <gio/gdesktopappinfo.h>
#define GNOME_DESKTOP_USE_UNSTABLE_API
#include <libgnome-desktop/gnome-wall-clock.h>
#include <bananui/bananui.h>
#include "cellular_manager.h"
#include "slot_dialog.h"
#include "slot_widget.h"
#include "shell.h"
#include "settings.h"
#include "layerwindow.h"
#include "window_manager.h"
#include "homescreen.h"

#define NUM_APP_COLUMNS 3
#define NUM_APP_ROWS_PER_PAGE 3

enum bsh_homescreen_state {
	HOMESCREEN_HOME,
	HOMESCREEN_OVERVIEW,
	HOMESCREEN_WINDOWLIST,
	HOMESCREEN_APPS
};

struct app_list_app {
	GList *page;
	unsigned int x, y;
	GAppInfo *app_info;
	bBox *box;
	bContent *icon;
};

struct app_list_page {
	bCard *card;
	struct app_list_app apps[NUM_APP_ROWS_PER_PAGE][NUM_APP_COLUMNS];
};

struct bsh_homescreen {
	bool active, showed_slot_dialog;
	struct wl_output *output;
	enum bsh_homescreen_state state;
	bWindow *wnd;
	bCard *home_card, *overview_card, *apps_card, *animated;
	bBox *overview_hints, *cellular_info;
	bContent *app_title;
	GList *pages;
	struct app_list_app *selected;
	GAppInfoMonitor *monitor;
	GnomeWallClock *wall_clock;
	unsigned long slot_handler;
	bContent *clock;
};

static inline bCard *get_selected_card(struct bsh_homescreen *hs)
{
	struct app_list_page *page = hs->selected->page->data;
	return page->card;
}

static int homescreen_render(void *param, void *data)
{
	struct bsh_homescreen *hs = data;
	if (hs->active && hs->state != HOMESCREEN_WINDOWLIST) {
		/* TODO show a real wallpaper */
		cairo_set_source_rgb(hs->wnd->renderer, 0.2, 0.2, 0.2);
		cairo_paint(hs->wnd->renderer);
	}
	if(hs->animated){
		bRenderCard(hs->wnd, hs->animated);
	}
	switch(hs->state){
		case HOMESCREEN_HOME:
			bRenderCard(hs->wnd, hs->home_card);
			break;
		case HOMESCREEN_OVERVIEW:
			bRenderCard(hs->wnd, hs->overview_card);
			break;
		case HOMESCREEN_WINDOWLIST:
			break;
		case HOMESCREEN_APPS:
			bRenderCard(hs->wnd, hs->apps_card);
			if(hs->selected){
				bRenderCard(hs->wnd, get_selected_card(hs));
			}
			break;
	}
	if(hs->animated && bAnimationComplete(hs->animated)){
		hs->animated = NULL;
	}
	return 1;
}

static int homescreen_open(struct bsh_homescreen *hs);

static void window_switcher_start(struct bsh_homescreen *hs)
{
	hs->state = HOMESCREEN_WINDOWLIST;
	bTranslateCard(hs->overview_card, -hs->wnd->width, 0, 0.2);
	hs->animated = hs->overview_card;
	bRequestFrame(hs->wnd);
}

static void set_selected_app(struct bsh_homescreen *hs,
		struct app_list_app *app)
{
	if (hs->selected)
		bBindStyle(hs->selected->box, "shell applist app");
	hs->selected = app;
	if (app) {
		bBindStyle(app->box, "shell applist app :selected");
		bSetText(hs->app_title,
			 g_app_info_get_display_name(app->app_info));
	}
}

static void handle_direction_key(struct bsh_homescreen *hs,
		xkb_keysym_t sym)
{
	struct app_list_app *app = hs->selected;
	struct app_list_page *page;
	unsigned int x, y;
	if(!app) return;
	page = app->page->data;
	x = app->x;
	y = app->y;
	if(sym == XKB_KEY_Up){
		if(y > 0){
			y--;
		}
		else if(app->page->next){
			/* pages are stored in reverse order */
			page = app->page->next->data;
			y = NUM_APP_ROWS_PER_PAGE-1;
			/*
			 * this page wouldn't exist if the previous page
			 * weren't full
			 */
			g_assert(page->apps[y][x].box);
		}
		else {
			/* find nearset app on last page with x <= app->x */
			page = hs->pages->data;
			for(y = NUM_APP_ROWS_PER_PAGE-1; ; y--){
				for(x = app->x; ; x--){
					if(page->apps[y][x].box)
						goto found;
					if(x == 0) break;
				}
				g_assert(y != 0); /* empty page? */
			}
		}
	}
	else if(sym == XKB_KEY_Down){
		if(y < NUM_APP_ROWS_PER_PAGE-1 && page->apps[y+1][0].box){
			y++;
		}
		else if(app->page->prev){
			/* pages are stored in reverse order */
			page = app->page->prev->data;
			y = 0;
		}
		else {
			/* go to first page */
			page = g_list_last(hs->pages)->data;
			y = 0;
			/* no row can be longer than the first row */
			g_assert(page->apps[y][x].box);
		}
		/* find nearest app with x <= app->x */
		for(x = app->x; ; x--){
			if(page->apps[y][x].box) break;
			g_assert(x != 0); /* empty page? */
		}
	}
	else if(sym == XKB_KEY_Left){
		if(x > 0){
			x--;
		}
		else if(y > 0){
			x = NUM_APP_COLUMNS-1;
			y--;
		}
		else if(app->page->next){
			/* pages are stored in reverse order */
			page = app->page->next->data;
			x = NUM_APP_COLUMNS-1;
			y = NUM_APP_ROWS_PER_PAGE-1;
			/*
			 * this page wouldn't exist if the previous page
			 * weren't full
			 */
			g_assert(page->apps[y][x].box);
		}
		else {
			/* find last app on last page */
			page = hs->pages->data;
			for(y = NUM_APP_ROWS_PER_PAGE-1; ; y--){
				for(x = NUM_APP_COLUMNS-1; ; x--){
					if(page->apps[y][x].box)
						goto found;
					if(x == 0) break;
				}
				g_assert(y != 0); /* empty page? */
			}
		}
	}
	else if(sym == XKB_KEY_Right){
		if(x < NUM_APP_COLUMNS-1 && page->apps[y][x+1].box){
			x++;
		}
		else if(y < NUM_APP_ROWS_PER_PAGE-1 && page->apps[y+1][0].box){
			x = 0;
			y++;
		}
		else if(app->page->prev){
			/* pages are stored in reverse order */
			page = app->page->prev->data;
			y = 0;
			x = 0;
			g_assert(page->apps[y][x].box); /* empty page? */
		}
		else {
			/* go to first page */
			page = g_list_last(hs->pages)->data;
			y = 0;
			x = 0;
			g_assert(page->apps[y][x].box); /* empty page? */
		}
	}
found:
	bAnimateCardWith(page->card, hs->apps_card);
	set_selected_app(hs, &page->apps[y][x]);
	bRequestFrame(hs->wnd);
}

static void handle_launch(struct bsh_homescreen *hs)
{
	BshShell *shell = bsh_shell_get_default();
	struct app_list_app *app = hs->selected;

	if(!window_manager_open_app(shell->window_manager, app->app_info)){
		GError *error = NULL;
		g_desktop_app_info_launch_uris_as_manager(
				G_DESKTOP_APP_INFO(app->app_info),
				NULL,
				shell->launch_context,
				G_SPAWN_SEARCH_PATH,
				NULL, NULL, NULL, NULL,
				&error);
		if(error){
			g_critical("Failed to launch app: %s",
					error->message);
		}
	}
	homescreen_hide(hs);
}

static int homescreen_handle_key(void *param, void *data)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_homescreen *hs = data;
	bKeyEvent *ev = param;

	if(hs->state == HOMESCREEN_HOME){
		if (ev->sym == XKB_KEY_1 ||
			ev->sym == XKB_KEY_2 ||
			ev->sym == XKB_KEY_3 ||
			ev->sym == XKB_KEY_4 ||
			ev->sym == XKB_KEY_5 ||
			ev->sym == XKB_KEY_6 ||
			ev->sym == XKB_KEY_7 ||
			ev->sym == XKB_KEY_8 ||
			ev->sym == XKB_KEY_9 ||
			ev->sym == BANANUI_KEY_Star ||
			ev->sym == XKB_KEY_0 ||
			ev->sym == BANANUI_KEY_Pound)
		{
			bsh_dialer_show(shell->dialer);
			bsh_dialer_key(shell->dialer, ev->sym);
		}
		else if(ev->sym == XKB_KEY_Up){
			settings_show(shell->settings);
		}
		else if(ev->sym == XKB_KEY_Down ||
				ev->sym == XKB_KEY_Return)
		{
			bTranslateCard(hs->home_card,
					0, -hs->wnd->height, 0.2);
			bFadeCard(hs->home_card, 0, 0.2);
			
			bTranslateCard(hs->apps_card,
					0, hs->wnd->height, 0);
			bTranslateCard(hs->apps_card, 0, 0, 0.2);
			bFadeCard(hs->apps_card, 1, 0.2);
			
			if(hs->selected){
				bAnimateCardWith(get_selected_card(hs),
						hs->apps_card);
			}
			hs->animated = hs->home_card;
			hs->state = HOMESCREEN_APPS;
			bRequestFrame(hs->wnd);
		}
	}
	else if (hs->state == HOMESCREEN_WINDOWLIST) {
		if(ev->sym == XKB_KEY_Up){
			window_manager_close(shell->window_manager);
		} else if (ev->sym == XKB_KEY_Right) {
			window_manager_go_next(shell->window_manager);
		} else if(ev->sym == XKB_KEY_Left) {
			window_manager_go_prev(shell->window_manager);
		} else if (ev->sym == XKB_KEY_Down) {
			homescreen_show(hs);
		} else if (ev->sym == XKB_KEY_Return) {
			if (window_manager_has_window(shell->window_manager)) {
				bTranslateCard(hs->home_card, 0, 0, 0);
				bFadeCard(hs->home_card, 1, 0);
				hs->state = HOMESCREEN_HOME;
				homescreen_hide(hs);
			}
		} else if (ev->sym == BANANUI_KEY_Back || ev->sym == BANANUI_KEY_EndCall) {
			bTranslateCard(hs->overview_card, 0, 0, 0.2);
			hs->state = HOMESCREEN_OVERVIEW;
			bRequestFrame(hs->wnd);
		}
	}
	else if (hs->state == HOMESCREEN_OVERVIEW) {
		if(ev->sym == XKB_KEY_Up){
			/*
			 * When overview is triggered from homescreen,
			 * we want to go back to homescreen state.
			 * Otherwise we want to hide our window.
			 */
			if(hs->active){
				homescreen_show(hs);
			}
			settings_show(shell->settings);
			if(!hs->active) {
				homescreen_hide(hs);
			}
		}
		else if(ev->sym == XKB_KEY_Down){
			homescreen_show(hs);
		}
		else if(ev->sym == XKB_KEY_Right){
			g_idle_add_once((GSourceOnceFunc)window_switcher_start, hs);
		}
		else if(ev->sym == BANANUI_KEY_Back || ev->sym == BANANUI_KEY_EndCall){
			if(hs->active){
				homescreen_show(hs);
			}
			else {
				homescreen_hide(hs);
			}
		}
	}
	else if(hs->state == HOMESCREEN_APPS){
		if(ev->sym == XKB_KEY_Left ||
				ev->sym == XKB_KEY_Right ||
				ev->sym == XKB_KEY_Up ||
				ev->sym == XKB_KEY_Down)
		{
			handle_direction_key(hs, ev->sym);
		}
		else if(ev->sym == XKB_KEY_Return)
		{
			handle_launch(hs);
			/* homescreen might be gone by now */
			return 0;
		}
		else if(ev->sym == BANANUI_KEY_Back ||
				ev->sym == BANANUI_KEY_EndCall)
		{
			bFadeCard(hs->apps_card, 0, 0.2);

			bTranslateCard(hs->home_card, 0, 0, 0);
			bFadeCard(hs->home_card, 1, 0.2);

			hs->animated = hs->apps_card;

			if(hs->pages){
				struct app_list_page *first_page =
					g_list_last(hs->pages)->data;
				set_selected_app(hs, &first_page->apps[0][0]);
			}
			hs->state = HOMESCREEN_HOME;
			bRequestFrame(hs->wnd);
		}
	}
	return 1;
}

void homescreen_hide(struct bsh_homescreen *hs)
{
	bWindow *wnd = hs->wnd;
	hs->active = false;
	if(!wnd) return;
	g_debug("Closing homescreen");
	hs->wnd = NULL;
	bDestroyWindow(wnd); /* does not return if inside event handler */
}

static int homescreen_open(struct bsh_homescreen *hs)
{
	if(hs->wnd) return 0;
	hs->wnd = bCreateLayerWindow("bananui-shell homescreen",
			hs->output, 0, 0, ZWLR_LAYER_SHELL_V1_LAYER_TOP,
			-30, 0, 0, 0,
			ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
			ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
			ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
			ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
			0, 1);
	if(!hs->wnd){
		g_critical("Failed to open homescreen window");
		return -1;
	}

	bRegisterEventHandler(&hs->wnd->keydown, homescreen_handle_key, hs);
	bRegisterEventHandler(&hs->wnd->redraw, homescreen_render, hs);
	return 0;
}

void homescreen_show(struct bsh_homescreen *hs)
{
	BshShell *shell = bsh_shell_get_default();

	settings_hide_now(shell->settings);
	
	g_debug("Opening homescreen");
	hs->active = true;

	if(homescreen_open(hs) < 0) return;

	switch (hs->state) {
	case HOMESCREEN_OVERVIEW:
		bFadeCard(hs->overview_card, 0, 0.2);
		/* fallthrough */
	case HOMESCREEN_WINDOWLIST:
		bTranslateCard(hs->home_card, 0, hs->wnd->height, 0);
		bTranslateCard(hs->home_card, 0, 0, 0.2);
		bFadeCard(hs->home_card, 1, 0.2);

		hs->animated = hs->overview_card;
		hs->state = HOMESCREEN_HOME;
		break;
	default:
		break;
	}

	bRequestFrame(hs->wnd);
}

void homescreen_show_overview(struct bsh_homescreen *hs)
{
	BshShell *shell = bsh_shell_get_default();

	settings_hide_now(shell->settings);
	if(hs->wnd && hs->state == HOMESCREEN_OVERVIEW) return;

	g_debug("Opening overview");

	if(homescreen_open(hs) < 0) return;

	hs->state = HOMESCREEN_OVERVIEW;
	bTranslateCard(hs->overview_card, 0, 0, 0);
	bFadeCard(hs->overview_card, 0, 0);
	bFadeCard(hs->overview_card, 1, 0.15);

	bRequestFrame(hs->wnd);
}

static void destroy_page(void *ptr)
{
	struct app_list_page *page = ptr;
	bDestroyCard(page->card);
}

static void app_create_widgets(struct app_list_app *app, bBox *row)
{
	bBox *iconbox = NULL;
	bContent *icon = NULL;
	GIcon *gicon = g_app_info_get_icon(app->app_info);
	app->box = bBuildBox(bDefaultTheme(NULL),
			     "shell applist app",
			     1,
			     &iconbox);
	if (!app->box) {
		/* placeholder */
		app->box = bCreateBox("shell applist app");
		bAddBox(row, app->box);
		return;
	}
	bAddBox(row, app->box);
	if (!iconbox)
		return;
	if (G_IS_THEMED_ICON(gicon)) {
		const char * const *names =
			g_themed_icon_get_names(G_THEMED_ICON(gicon));
		for (; *names && !icon; names++) {
			icon = bCreateIcon(*names, 56);
		}
	}
	else if (G_IS_FILE_ICON(gicon)) {
		GFile *file = g_file_icon_get_file(G_FILE_ICON(gicon));
		char *path = g_file_get_path(file);
		icon = bCreateIcon(path, 56);
		if (!icon)
			g_warning("Failed to load image %s\n", path);
		g_free(path);
	}
	if (!icon) {
		icon = bCreateIcon("image-missing", 56);
		if (!icon) {
			g_warning("Failed to load image-missing icon");
			return;
		}
	}
	bAddContent(iconbox, icon);
}

static void regenerate_applist(GAppInfoMonitor *monitor,
		struct bsh_homescreen *hs)
{
	struct app_list_page *page = NULL;
	struct app_list_app *first_app = NULL;
	GList *apps, *app;
	bBox *rows = NULL, *row = NULL;
	unsigned int x = 0, y = 0;

	set_selected_app(hs, NULL);
	hs->animated = NULL;
	g_clear_list(&hs->pages, destroy_page);

	apps = g_app_info_get_all();
	for (app = apps; app; app = app->next) {
		if (!g_app_info_should_show(G_APP_INFO(app->data)))
			continue;
		if (!page) {
			page = g_new0(struct app_list_page, 1);
			page->card = bBuildCard(bDefaultTheme(NULL),
						"shell applist page",
						1,
						&rows);
			if (!page->card)
				page->card = bCreateCard("shell applist page");
			if (!rows)
				bAddBox(page->card->box,
					rows = bCreateBox("shell applist apps"));
			hs->pages = g_list_prepend(hs->pages, page);
			x = 0;
			y = 0;
		}
		if (x == 0) {
			bAddBox(rows, row = bCreateBox("shell applist row"));
		}
		page->apps[y][x].page = hs->pages;
		page->apps[y][x].x = x;
		page->apps[y][x].y = y;
		page->apps[y][x].app_info = G_APP_INFO(app->data);
		app_create_widgets(&page->apps[y][x], row);
		if (!first_app)
			first_app = &page->apps[y][x];
		x++;
		if (x >= NUM_APP_COLUMNS) {
			x = 0;
			y++;
			if (y >= NUM_APP_ROWS_PER_PAGE) {
				page = NULL;
			}
		}
	}
	if (first_app)
		set_selected_app(hs, first_app);
	if (hs->wnd)
		bRequestFrame(hs->wnd);
	g_list_free(apps);
}

static void handle_clock_update(GnomeWallClock *clock,
		GParamSpec *pspec, struct bsh_homescreen *hs)
{
	bSetText(hs->clock, gnome_wall_clock_get_clock(clock));
	if(hs->wnd) bRequestFrame(hs->wnd);
}

static void
refresh (struct bsh_homescreen *hs)
{
  if (hs->wnd)
    bRequestFrame (hs->wnd);
}

static void
handle_slots (BshCellularManager    *cm,
              GParamSpec            *pspec,
              struct bsh_homescreen *hs)
{
  g_autoptr(GPtrArray) slots = bsh_cellular_manager_get_sim_slots (cm);
  unsigned int         i;

  g_debug ("Slots updated");

  if (!hs->showed_slot_dialog && bsh_cellular_manager_is_modem_locked (cm)) {
    BshSlotDialog *dlg = bsh_slot_dialog_new (hs->output, FALSE);
    bsh_slot_dialog_show (dlg);
    g_object_unref (dlg);
    hs->showed_slot_dialog = true;
  }

  if (!hs->cellular_info)
    return;

  bDestroyBoxChildren (hs->cellular_info);

  for (i = 0; i < slots->len; i++) {
    WroomdDBusWroomdSlot *slot = g_ptr_array_index (slots, i);
    BshSlotWidget        *widget =
      bsh_slot_widget_new (slot, BSH_SLOT_WIDGET_FLAG_NONE);

    g_signal_connect_swapped (widget, "updated", G_CALLBACK(refresh), hs);

    bAddBox (hs->cellular_info, bsh_slot_widget_into_box (widget));
  }

  if (hs->wnd)
    bRequestFrame (hs->wnd);
}

static void overview_add_hint(struct bsh_homescreen *hs, const char *sym,
		const char *label)
{
	bBox *row, *sym_box, *label_box;
	bAddBox(hs->overview_hints,
		row = bCreateBox("shell overview hint"));
	bAddBox(row,
		sym_box = bCreateBox("shell overview hint icon"));
	if(sym[0] == '@')
		bAddContent(sym_box, bCreateIcon(sym+1, 16));
	else
		bAddContent(sym_box, bCreateText(sym));
	bAddBox(row,
		label_box = bCreateBox("shell overview hint label"));
	bAddContent(label_box, bCreateText(label));
}

struct bsh_homescreen *create_homescreen(struct wl_output *output)
{
	BshCellularManager *cm = bsh_cellular_manager_get_default();
	bBox *home_boxes[2] = { 0 }, *apps_boxes[1] = { 0 };
	struct bsh_homescreen *hs = g_new0(struct bsh_homescreen, 1);

	hs->output = output;

	hs->wall_clock = gnome_wall_clock_new();
	g_object_set(hs->wall_clock, "time-only", TRUE, NULL);

	hs->home_card = bBuildCard(bDefaultTheme(NULL),
				   "shell homescreen",
				   2,
				   home_boxes);
	if (!hs->home_card)
		g_error("Failed to build home card");
	if (home_boxes[0])
		bAddContent(home_boxes[0],
			    hs->clock = bCreateText(
				    gnome_wall_clock_get_clock(hs->wall_clock)));
	hs->cellular_info = home_boxes[1];

	hs->slot_handler = g_signal_connect(cm, "notify::slots",
			G_CALLBACK(handle_slots), hs);
	handle_slots(cm, NULL, hs);
	g_signal_connect(hs->wall_clock, "notify::clock",
			G_CALLBACK(handle_clock_update), hs);

	hs->overview_card = bBuildCard(bDefaultTheme(NULL),
				       "shell overview",
				       1,
				       &hs->overview_hints);
	if (!hs->overview_card)
		g_error("Failed to build overview card");
	overview_add_hint(hs, "@go-up-symbolic", "Settings");
	overview_add_hint(hs, "@go-next-symbolic", "See open apps");
	overview_add_hint(hs, "@go-down-symbolic", "Home");

	hs->apps_card = bBuildCard(bDefaultTheme(NULL),
				   "shell applist",
				   1,
				   apps_boxes);
	if (!hs->apps_card)
		g_error("Failed to build apps card");
	if (apps_boxes[0])
		bAddContent(apps_boxes[0],
			    hs->app_title = bCreateText("No apps"));

	regenerate_applist(NULL, hs);

	hs->monitor = g_app_info_monitor_get();
	g_signal_connect(hs->monitor, "changed",
			G_CALLBACK(regenerate_applist), hs);

	hs->state = HOMESCREEN_HOME;
	bFadeCard(hs->home_card, 0, 0);
	bFadeCard(hs->home_card, 1, 0.2);
	bFadeCard(hs->apps_card, 0, 0);
	bFadeCard(hs->overview_card, 0, 0);

	return hs;
}

void destroy_homescreen(struct bsh_homescreen *hs)
{
	BshCellularManager *cm = bsh_cellular_manager_get_default();
	g_clear_list(&hs->pages, destroy_page);
	g_object_unref(hs->wall_clock);
	g_signal_handler_disconnect(cm, hs->slot_handler);
	homescreen_hide(hs);
	bDestroyCard(hs->home_card);
	bDestroyCard(hs->overview_card);
	bDestroyCard(hs->apps_card);
	g_object_unref(hs->monitor);
	g_free(hs);
}

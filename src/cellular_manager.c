/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-cellular-manager"

#include "call.h"
#include "cellular_manager.h"
#include "dbus/modem-dbus.h"

enum {
  BSH_CELLULAR_MANAGER_PROP_0,
  BSH_CELLULAR_MANAGER_PROP_SLOTS,
  BSH_CELLULAR_MANAGER_PROP_LAST_PROP,
};
static GParamSpec *props[BSH_CELLULAR_MANAGER_PROP_LAST_PROP];

struct _BshCellularManager {
  GObject parent;

  unsigned int        watch_id;
  GDBusObjectManager *object_manager, *config_object_manager;
  GHashTable         *slots, *calls, *slot_config;
};

G_DEFINE_TYPE (BshCellularManager, bsh_cellular_manager, G_TYPE_OBJECT)


static void
bsh_cellular_manager_get_property (GObject    *object,
                                   guint       property_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  BshCellularManager *self = BSH_CELLULAR_MANAGER (object);

  switch (property_id) {
  case BSH_CELLULAR_MANAGER_PROP_SLOTS:
    g_value_set_boxed (value, self->slots);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
wroomd_interface_added_cb (BshCellularManager *self,
                           GDBusObject        *object,
                           GDBusInterface     *iface)
{
  const char *path = g_dbus_object_get_object_path (object);
  if (WROOMD_DBUS_IS_WROOMD_SLOT(iface)) {
    g_debug ("Found slot at %s", path);
    g_hash_table_insert (self->slots, g_strdup (path), g_object_ref (iface));
    g_object_notify_by_pspec (G_OBJECT(self),
                              props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
  } else if (WROOMD_DBUS_IS_WROOMD_CALL(iface)) {
    g_debug ("Found call at %s", path);
    g_hash_table_insert (self->calls,
                         g_strdup (path),
                         bsh_call_new (WROOMD_DBUS_WROOMD_CALL(iface)));
  }
}


static void
wroomd_interface_removed_cb (BshCellularManager *self,
                             GDBusObject        *object,
                             GDBusInterface     *iface)
{
  const char *path = g_dbus_object_get_object_path (object);
  if (WROOMD_DBUS_IS_WROOMD_SLOT(iface)) {
    g_debug ("Removed slot at %s", path);
    if (g_hash_table_remove (self->slots, path)) {
      g_object_notify_by_pspec (G_OBJECT(self),
                                props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
    }
  } else if (WROOMD_DBUS_IS_WROOMD_CALL(iface)) {
    g_debug ("Removed call at %s", path);
    g_hash_table_remove (self->calls, path);
  }
}


static void
wroomd_object_added_cb (BshCellularManager *self,
                        GDBusObject        *object)
{
  GList *ifaces, *iface;

  ifaces = g_dbus_object_get_interfaces (object);
  for (iface = ifaces; iface; iface = iface->next) {
    wroomd_interface_added_cb (self, object, G_DBUS_INTERFACE(iface->data));
  }
}


static void
wroomd_object_removed_cb (BshCellularManager *self,
                          GDBusObject        *object)
{
  const char *path = g_dbus_object_get_object_path (object);
  g_debug ("Object %s removed", path);
  if (g_hash_table_remove (self->slots, path)) {
    g_object_notify_by_pspec (G_OBJECT(self),
                              props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
  }
  g_hash_table_remove (self->calls, path);
}


static void
add_wroomd_objects (BshCellularManager *self)
{
  GList *objects, *object;

  objects = g_dbus_object_manager_get_objects (self->object_manager);
  for (object = objects; object; object = object->next) {
    wroomd_object_added_cb (self, G_DBUS_OBJECT (object->data));
  }

  g_list_free_full (objects, g_object_unref);
}


static void
wroomd_object_manager_cb (GDBusConnection    *connection,
                          GAsyncResult       *res,
                          BshCellularManager *self)
{
  g_autoptr(GError) error = NULL;

  self->object_manager = wroomd_dbus_object_manager_client_new_for_bus_finish (res, &error);
  if (!self->object_manager) {
    g_warning ("Error creating object manager client: %s", error->message);
    return;
  }

  g_debug ("Got wroomd object manager");

  g_signal_connect_swapped (self->object_manager,
                            "interface-added",
                            G_CALLBACK (wroomd_interface_added_cb), self);
  g_signal_connect_swapped (self->object_manager,
                            "interface-removed",
                            G_CALLBACK (wroomd_interface_removed_cb), self);
  g_signal_connect_swapped (self->object_manager,
                            "object-added",
                            G_CALLBACK (wroomd_object_added_cb), self);
  g_signal_connect_swapped (self->object_manager,
                            "object-removed",
                            G_CALLBACK (wroomd_object_removed_cb), self);

  add_wroomd_objects (self);
}


static void
name_changed_cb (BshCellularManager *self)
{
  g_object_notify_by_pspec (G_OBJECT(self),
                            props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
}


static void
config_object_added_cb (BshCellularManager *self,
                        GDBusObject        *object)
{
  GDBusInterface *interface;
  const char     *path = g_dbus_object_get_object_path (object);

  interface = g_dbus_object_get_interface (object, "de.abscue.obp.Bananui.SIMSlot");
  if (BMODEM_DBUS_IS_SIMSLOT(interface)) {
    BModemDBusSIMSlot *slot        = BMODEM_DBUS_SIMSLOT(interface);
    const char        *wroomd_path = bmodem_dbus_simslot_get_wroomd_path (slot);

    g_debug ("Found configuration for %s at %s", wroomd_path, path);
    g_hash_table_insert (self->slot_config,
		    	 g_strdup (wroomd_path),
			 g_object_ref (interface));
    g_object_notify_by_pspec (G_OBJECT(self),
                              props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
    g_signal_connect_object (slot,
                             "notify::name",
                             G_CALLBACK(name_changed_cb),
                             G_OBJECT(self),
                             G_CONNECT_SWAPPED);
  }
}


static void
config_object_removed_cb (BshCellularManager *self,
                          GDBusObject        *object)
{
  GDBusInterface *interface;
  const char     *path = g_dbus_object_get_object_path (object);

  interface = g_dbus_object_get_interface (object, "de.abscue.obp.Bananui.SIMSlot");
  if (BMODEM_DBUS_IS_SIMSLOT(interface)) {
    BModemDBusSIMSlot *slot        = BMODEM_DBUS_SIMSLOT(interface);
    const char        *wroomd_path = bmodem_dbus_simslot_get_wroomd_path (slot);

    g_debug ("Configuration for %s at %s removed", wroomd_path, path);
    /* wroomd_path is not expected to change */
    g_hash_table_remove (self->slot_config, wroomd_path);
    g_object_notify_by_pspec (G_OBJECT(self),
                              props[BSH_CELLULAR_MANAGER_PROP_SLOTS]);
  }
}


static void
add_config_objects (BshCellularManager *self)
{
  GList *objects, *object;

  objects = g_dbus_object_manager_get_objects (self->config_object_manager);
  for (object = objects; object; object = object->next) {
    config_object_added_cb (self, G_DBUS_OBJECT (object->data));
  }

  g_list_free_full (objects, g_object_unref);
}


static void
config_object_manager_cb (GDBusConnection    *connection,
                          GAsyncResult       *res,
                          BshCellularManager *self)
{
  g_autoptr(GError) error = NULL;

  self->config_object_manager =
    bmodem_dbus_object_manager_client_new_for_bus_finish (res, &error);
  if (!self->config_object_manager) {
    g_warning ("Error creating object manager client: %s", error->message);
    return;
  }

  g_debug ("Got configuration object manager");

  g_signal_connect_swapped (self->config_object_manager,
                            "object-added",
                            G_CALLBACK (config_object_added_cb), self);
  g_signal_connect_swapped (self->config_object_manager,
                            "object-removed",
                            G_CALLBACK (config_object_removed_cb), self);

  add_config_objects (self);
}


static void
bsh_cellular_manager_constructed (GObject *object)
{
  BshCellularManager *self = BSH_CELLULAR_MANAGER (object);

  G_OBJECT_CLASS (bsh_cellular_manager_parent_class)->constructed (object);

  wroomd_dbus_object_manager_client_new_for_bus (
    G_BUS_TYPE_SYSTEM,
    G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE,
    "de.abscue.obp.Wroomd",
    "/de/abscue/obp/Wroomd",
    NULL,
    (GAsyncReadyCallback) wroomd_object_manager_cb,
    self);

  bmodem_dbus_object_manager_client_new_for_bus (
    G_BUS_TYPE_SESSION,
    G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE,
    "de.abscue.obp.Bananui.Modem",
    "/de/abscue/obp/Bananui/SIMSlot",
    NULL,
    (GAsyncReadyCallback) config_object_manager_cb,
    self);
}


static void
bsh_cellular_manager_dispose (GObject *object)
{
  BshCellularManager *self = BSH_CELLULAR_MANAGER (object);

  g_object_unref (self->object_manager);
  g_hash_table_unref (self->slots);

  G_OBJECT_CLASS (bsh_cellular_manager_parent_class)->dispose (object);
}


static void
bsh_cellular_manager_class_init (BshCellularManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bsh_cellular_manager_constructed;
  object_class->dispose = bsh_cellular_manager_dispose;

  object_class->get_property = bsh_cellular_manager_get_property;

  props[BSH_CELLULAR_MANAGER_PROP_SLOTS] =
    g_param_spec_boxed ("slots", NULL, NULL,
                        G_TYPE_HASH_TABLE,
                        G_PARAM_READABLE);

  g_object_class_install_properties (object_class, BSH_CELLULAR_MANAGER_PROP_LAST_PROP, props);
}


static void
bsh_cellular_manager_init (BshCellularManager *self)
{
  self->slots = g_hash_table_new_full (g_str_hash,
                                       g_str_equal,
                                       g_free,
                                       g_object_unref);
  self->calls = g_hash_table_new_full (g_str_hash,
                                       g_str_equal,
                                       g_free,
                                       g_object_unref);
  self->slot_config = g_hash_table_new_full (g_str_hash,
                                             g_str_equal,
                                             g_free,
                                             g_object_unref);
}


BshCellularManager *
bsh_cellular_manager_get_default (void)
{
  static BshCellularManager *instance;

  if (instance == NULL) {
    instance = g_object_new (BSH_TYPE_CELLULAR_MANAGER, NULL);
    g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *)&instance);
  }
  return instance;
}


const char *
bsh_cellular_manager_get_slot_name (BshCellularManager   *self,
                                    WroomdDBusWroomdSlot *slot)
{
  GDBusObject       *obj  = g_dbus_interface_get_object (G_DBUS_INTERFACE(slot));
  const char        *path = g_dbus_object_get_object_path (obj);
  BModemDBusSIMSlot *conf = g_hash_table_lookup (self->slot_config, path);

  return conf ? bmodem_dbus_simslot_get_name (conf) : path;
}

static int
compare_slot_paths (gconstpointer a, gconstpointer b, gpointer user_data)
{
  BshCellularManager   *self          = user_data;
  WroomdDBusWroomdSlot *const *slot_a = a;
  WroomdDBusWroomdSlot *const *slot_b = b;

  return strcmp (
    bsh_cellular_manager_get_slot_name (self, *slot_a),
    bsh_cellular_manager_get_slot_name (self, *slot_b));
}


GPtrArray *
bsh_cellular_manager_get_sim_slots (BshCellularManager *self)
{
  GPtrArray      *slots = g_ptr_array_new ();
  GHashTableIter  slot_iter;
  void *path;
  void *slot;

  g_hash_table_iter_init (&slot_iter, self->slots);
  while (g_hash_table_iter_next (&slot_iter, &path, &slot)) {
    g_ptr_array_add (slots, slot);
  }

  g_ptr_array_sort_with_data (slots, compare_slot_paths, self);

  return slots;
}


gboolean
bsh_cellular_manager_is_modem_locked (BshCellularManager *self)
{
  GHashTableIter slot_iter;
  void *path;
  void *slot;

  g_hash_table_iter_init (&slot_iter, self->slots);
  while (g_hash_table_iter_next (&slot_iter, &path, &slot)) {
    if (0 != strcmp ("none", wroomd_dbus_wroomd_slot_get_lock (slot)))
      return TRUE;
  }

  return FALSE;
}


void
bsh_cellular_manager_hangup_calls (BshCellularManager *self)
{
  GHashTableIter iter;
  void *path;
  void *call;

  g_hash_table_iter_init (&iter, self->calls);
  while (g_hash_table_iter_next (&iter, &path, &call))
    bsh_call_hangup (call);
}


void
bsh_cellular_manager_accept_calls (BshCellularManager *self)
{
  GHashTableIter iter;
  void *path;
  void *call;

  g_hash_table_iter_init (&iter, self->calls);
  while (g_hash_table_iter_next (&iter, &path, &call))
    bsh_call_accept (call);
}


/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-notification"

#define LIBFEEDBACK_USE_UNSTABLE_API

#include "notification.h"

#include <bananui/bananui.h>
#include <libfeedback.h>
#include "layerwindow.h"

enum {
  PROP_0,
  PROP_URGENT,
  PROP_EVENT,
  N_PROPS,
};
static GParamSpec *props[N_PROPS];

typedef struct _BshNotification
{
  GObject parent;

  bWindow *wnd;
  bCard *card;
  bContent *popup_icon;
  bContent *popup_summary;
  bContent *popup_body;
  unsigned int timeout;
  gboolean urgent;
  char *event;
  LfbEvent *fb;
} BshNotification;

G_DEFINE_TYPE (BshNotification, bsh_notification, G_TYPE_OBJECT);


static void
close_popup (gpointer data)
{
  BshNotification *self = data;

  self->timeout = 0;
  g_clear_pointer (&self->wnd, bDestroyWindow);
  g_clear_pointer (&self->card, bDestroyCard);
}


static int
render_notification (void *param, void *data)
{
  BshNotification *self = data;

  bRenderCard (self->wnd, self->card);
  if (!self->urgent && !self->timeout && bAnimationComplete (self->card))
    self->timeout = g_idle_add_once (close_popup, self);

  return 1;
}


static void
fadeout_popup (gpointer data)
{
  BshNotification *self = data;

  self->urgent = FALSE;
  self->timeout = 0;
  bFadeCard (self->card, 0, 0.2);
  bRequestFrame (self->wnd);
}


static int
handle_key (void *param, void *data)
{
  BshNotification *self = data;

  if (self->fb)
    lfb_event_end_feedback_async (self->fb, NULL, NULL, NULL);
  g_idle_add_once (fadeout_popup, self);
  bUnregisterEventHandler (&self->wnd->keyup, handle_key, self);

  return 0;
}


static void
unblock_urgent (gpointer data)
{
  BshNotification *self = data;

  self->timeout = 0;
  bRegisterEventHandler (&self->wnd->keyup, handle_key, self);
  bFocusCard (self->wnd, self->card);
}


static void
bsh_notification_constructed (GObject *object)
{
  BshNotification *self = BSH_NOTIFICATION(object);
  bBox *boxes[3] = { 0 };

  if (self->event) {
    self->fb = lfb_event_new (self->event);
    /* repeat if urgent, play once if not */
    lfb_event_set_timeout (self->fb, self->urgent ? 0 : -1);
    lfb_event_trigger_feedback_async (self->fb, NULL, NULL, NULL);
  }

  self->wnd = bCreateLayerWindow ("bananui-shell notification",
                                  NULL, 0, self->urgent ? 0 : 60,
				  ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
                                  -30, 0, 0, 0,
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
				  ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
                                  0, self->urgent);
  self->card = bBuildCard (bDefaultTheme (NULL),
                           "shell notification popup",
                           3,
                           boxes);

  bFadeCard (self->card, 0, 0);
  bTranslateCard (self->card, -320, 0, 0);
  bFadeCard (self->card, 1, 0.2);
  bTranslateCard (self->card, 0, 0, 0.2);

  if (boxes[0])
    bAddContent (boxes[0], self->popup_icon = bCreateEmpty ());
  if (boxes[1])
    bAddContent (boxes[1], self->popup_summary = bCreateEmpty ());
  if (boxes[2])
    bAddContent (boxes[2], self->popup_body = bCreateEmpty ());
  bRegisterEventHandler(&self->wnd->redraw, render_notification, self);


  if (self->urgent) {
    self->timeout = g_timeout_add_once (4000, unblock_urgent, self);
  } else {
    self->timeout = g_timeout_add_once (4000, fadeout_popup, self);
  }
}


static void
bsh_notification_dispose (GObject *object)
{
  BshNotification *self = BSH_NOTIFICATION(object);

  g_free (self->event);

  if (self->timeout)
    g_source_remove (self->timeout);

  close_popup (self);
}


static void
bsh_notification_set_property (GObject *object,
                               guint property_id,
                               const GValue *value,
                               GParamSpec *pspec)
{
  BshNotification *self = BSH_NOTIFICATION (object);

  switch (property_id) {
  case PROP_URGENT:
    self->urgent = g_value_get_boolean (value);
    break;
  case PROP_EVENT:
    self->event = g_value_dup_string (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_notification_class_init (BshNotificationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bsh_notification_constructed;
  object_class->set_property = bsh_notification_set_property;
  object_class->dispose = bsh_notification_dispose;

  props[PROP_URGENT] =
    g_param_spec_boolean ("urgent",
                          "Urgent",
                          "Whether the popup is urgent",
			  FALSE,
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS);

  props[PROP_EVENT] =
    g_param_spec_string ("event",
                         "feedbackd event name",
                         "Name of event to trigger for this nofitication",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, props);
}


static void
bsh_notification_init (BshNotification *self)
{
}


BshNotification *
bsh_notification_new (const char *event, gboolean urgent)
{
  return g_object_new (BSH_TYPE_NOTIFICATION,
                       "event", event,
                       "urgent", urgent,
                       NULL);
}


void
bsh_notification_set (BshNotification *self,
                      const char *summary,
                      const char *body,
                      const char *icon)
{
  if (!self->wnd)
    return;

  if (self->popup_summary) {
    if (summary[0])
      bSetText (self->popup_summary, summary);
    else
      bClearContent (self->popup_summary);
  }
  if (self->popup_body) {
    if (body[0])
      bSetText (self->popup_body, body);
    else
      bClearContent (self->popup_body);
  }
  if (self->popup_icon) {
    if (icon[0])
      bSetIcon (self->popup_icon, icon, 32);
    else
      bClearContent (self->popup_icon);
  }

  bRequestFrame(self->wnd);
}

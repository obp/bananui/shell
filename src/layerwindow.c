/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "layer-window"

#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <glib.h>
#include <bananui/window.h>
#include "util.h"
#include "layerwindow.h"
#include "text-input-unstable-v3-client-protocol.h"

struct registry_interfaces {
	struct zwp_text_input_manager_v3 *text_input;
	struct zwlr_layer_shell_v1 *layer_shell;
	struct wl_compositor *comp;
	struct wl_seat *seat;
	struct wl_shm *shm;
};

typedef struct bsLayerWindow {
	struct wl_display *disp;
	struct wl_surface *wlsurf;
	struct wl_buffer *buffer;
	struct wl_shm_pool *pool;
	cairo_surface_t *surf;
	unsigned char *pixels;
	int shmfd;
	struct zwlr_layer_surface_v1 *layersurf;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_context *xkbctx;
	struct wl_callback *frame_callback;
	int in_frame_callback;
	struct registry_interfaces reg;
	int configured;
} bLayerWindow;

static void keyboard_handle_keymap(void *data, struct wl_keyboard *kb,
		uint32_t format, int32_t fd, uint32_t size)
{
	bWindow *wnd = data;
	bLayerWindow *ww = wnd->private;
	char *buf;
	if(format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1){
		g_critical("Incompatible keymap format");
		return;
	}
	buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if(buf == MAP_FAILED){
		g_critical("Failed to map keymap: %s", strerror(errno));
		return;
	}
	xkb_keymap_unref(ww->keymap);
	ww->keymap = xkb_keymap_new_from_buffer(ww->xkbctx,
		buf, size-1, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	if(!ww->keymap){
		munmap(buf, size);
		g_critical("Failed to load keymap");
		return;
	}
	munmap(buf, size);
	wnd->xkbstate = xkb_state_new(ww->keymap);
	if(!wnd->xkbstate){
		xkb_keymap_unref(ww->keymap);
		g_critical("Failed to create keyboard state");
		return;
	}
	g_debug("Loaded keymap");
}
static void keyboard_handle_enter(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface,
		struct wl_array *keys)
{
	bWindow *wnd = data;
	bLayerWindow *ww = wnd->private;
	if(ww->wlsurf != surface) return;
	g_debug("Keyboard entered %p", wnd);
	wnd->focused = 1;
	bTriggerEventSafe(wnd->focusin, &wnd->destroy_guard, wnd);
}
static void keyboard_handle_leave(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface)
{
	bWindow *wnd = data;
	bLayerWindow *ww = wnd->private;
	if(ww->wlsurf != surface) return;
	g_debug("Keyboard left %p", wnd);
	wnd->focused = 0;
	bStopKeyRepeat(wnd);
	bTriggerEventSafe(wnd->focusout, &wnd->destroy_guard, wnd);
}
static void keyboard_handle_key(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
	bWindow *wnd = data;
	if(!wnd->focused) return;
	g_debug("Key %s on %p", state == WL_KEYBOARD_KEY_STATE_PRESSED
			? "down" : "up", wnd);
	bWindowKey(wnd, key + 8, state == WL_KEYBOARD_KEY_STATE_PRESSED
			? XKB_KEY_DOWN : XKB_KEY_UP);
}
static void keyboard_handle_modifiers(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t depressed,
		uint32_t latched, uint32_t locked, uint32_t group)
{
	bWindow *wnd = data;
	xkb_state_update_mask(wnd->xkbstate, depressed, latched, locked,
		group, 0, 0);
}
static void keyboard_handle_repeat_info(void *data, struct wl_keyboard *kb,
		int32_t rate, int32_t delay)
{
	bWindow *wnd = data;
	wnd->key_repeat_rate = rate;
	wnd->key_repeat_delay = delay;
}

static const struct wl_keyboard_listener keyboard_listener = {
	.keymap = keyboard_handle_keymap,
	.enter = keyboard_handle_enter,
	.leave = keyboard_handle_leave,
	.key = keyboard_handle_key,
	.modifiers = keyboard_handle_modifiers,
	.repeat_info = keyboard_handle_repeat_info,
};

static void layer_surface_handle_configure(void *data,
		struct zwlr_layer_surface_v1 *layer_surface,
		uint32_t serial, uint32_t width, uint32_t height)
{
	bWindow *wnd = data;
	bLayerWindow *ww = wnd->private;
	if(ww->configured) return;
	if(!height) height = 290;
	if(!width) width = 240;
	wnd->height = height;
	wnd->width = width;
	ww->configured = 1;
	zwlr_layer_surface_v1_ack_configure(layer_surface, serial);
}
static void layer_surface_handle_closed(void *data,
		struct zwlr_layer_surface_v1 *layer_surface)
{
	bWindow *wnd = data;
	wnd->closing = 1;
}

static const struct zwlr_layer_surface_v1_listener layer_surface_listener = {
	.configure = layer_surface_handle_configure,
	.closed = layer_surface_handle_closed,
};

static void registry_handle_global(void *data, struct wl_registry *reg,
	uint32_t name, const char *interface, uint32_t version)
{
	struct registry_interfaces *interfaces = data;
	if(strcmp(interface, "wl_compositor") == 0){
		interfaces->comp = wl_registry_bind(reg, name,
			&wl_compositor_interface, version > 4 ? 4 : version);
	}
	else if(strcmp(interface, "wl_seat") == 0){
		interfaces->seat = wl_registry_bind(reg, name,
			&wl_seat_interface, version);
	}
	else if(strcmp(interface, "zwlr_layer_shell_v1") == 0){
		interfaces->layer_shell = wl_registry_bind(reg, name,
			&zwlr_layer_shell_v1_interface, version);
	}
	else if(strcmp(interface, "wl_shm") == 0){
		interfaces->shm = wl_registry_bind(reg, name,
			&wl_shm_interface, 1);
	}
	else if(strcmp(interface, "zwp_text_input_manager_v3") == 0){
		interfaces->text_input = wl_registry_bind(reg, name,
			&zwp_text_input_manager_v3_interface, version);
	}
}
static void registry_handle_global_remove(void *data, struct wl_registry *reg,
	uint32_t name)
{
}

static const struct wl_registry_listener listeners = {
	.global = registry_handle_global,
	.global_remove = registry_handle_global_remove,
};

static void handle_surface_frame(void *data, struct wl_callback *cb,
		uint32_t callback_data)
{
	bWindow *wnd = data;
	bLayerWindow *ww = wnd->private;
	wl_callback_destroy(cb);
	ww->frame_callback = NULL;
	ww->in_frame_callback = 1;

	/* Clear window to make it transparent */
	cairo_save(wnd->renderer);
	cairo_set_operator(wnd->renderer, CAIRO_OPERATOR_CLEAR);
	cairo_set_source_rgba(wnd->renderer, 0, 0, 0, 0);
	cairo_paint(wnd->renderer);
	cairo_restore(wnd->renderer);

	/* Request re-render */
	if(bTriggerEventSafe(wnd->redraw, &wnd->destroy_guard, wnd) < 0)
		return;

	/* Commit wayland surface */
	wl_surface_attach(ww->wlsurf, ww->buffer, 0, 0);
	wl_surface_damage(ww->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(ww->wlsurf);
	wl_display_flush(ww->disp);

	ww->in_frame_callback = 0;
}

static const struct wl_callback_listener frame_listener = {
	.done = handle_surface_frame,
};

static void layer_wnd_destroy(bWindow *wnd)
{
	bLayerWindow *ww = wnd->private;
	int stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,
				wnd->width);
	if(ww->frame_callback) wl_callback_destroy(ww->frame_callback);
	if(ww->keyboard) wl_keyboard_destroy(ww->keyboard);
	if(wnd->renderer) cairo_destroy(wnd->renderer);
	if(ww->surf) cairo_surface_destroy(ww->surf);
	if(ww->buffer) wl_buffer_destroy(ww->buffer);
	if(ww->pool) wl_shm_pool_destroy(ww->pool);
	if(ww->pixels) munmap(ww->pixels, stride * wnd->height);
	if(ww->layersurf) zwlr_layer_surface_v1_destroy(ww->layersurf);
	if(ww->wlsurf) wl_surface_destroy(ww->wlsurf);
	if(ww->shmfd >= 0) close(ww->shmfd);
	if(ww->reg.text_input)
		zwp_text_input_manager_v3_destroy(ww->reg.text_input);
	if(ww->reg.seat) wl_seat_destroy(ww->reg.seat);
	if(ww->reg.shm) wl_shm_destroy(ww->reg.shm);
	if(ww->disp) wl_display_flush(ww->disp);
	g_free(ww);
	wnd->private = NULL;
}

static int layer_wnd_init(bWindow *wnd, const char *title,
		const char *app_id, int fullscreen, int w, int h)
{
	/* we need more parameters, initialize later in bCreateLayerWindow */
	return 0;
}

static void layer_wnd_request_frame(bWindow *wnd)
{
	bLayerWindow *ww = wnd->private;
	if(ww->frame_callback) return;
	ww->frame_callback = wl_surface_frame(ww->wlsurf);
	wl_callback_add_listener(ww->frame_callback, &frame_listener, wnd);
	if(!ww->in_frame_callback){
		wl_surface_commit(ww->wlsurf);
		wl_display_flush(ww->disp);
	}
}

static bInputMethod *layer_wnd_get_input_method(bWindow *wnd)
{
	struct zwp_text_input_v3 *text_input;
	bLayerWindow *ww = wnd->private;
	if(!ww->reg.text_input) return NULL;
	text_input = zwp_text_input_manager_v3_get_text_input(
				ww->reg.text_input,
				ww->reg.seat);
	return bCreateWaylandInputMethod(ww->disp, text_input);
}

static const bWindowOps layer_wnd_ops = {
	.init = layer_wnd_init,
	.destroy = layer_wnd_destroy,
	.request_frame = layer_wnd_request_frame,
	.get_input_method = layer_wnd_get_input_method
};

bWindow *bCreateLayerWindow(const char *namespace,
		struct wl_output *output,
		int w, int h, int layer,
		int margin_top, int margin_left,
		int margin_bottom, int margin_right,
		unsigned int anchor, int exclusive_zone,
		unsigned int keyboard_interactivity)
{
	int stride;
	struct wl_registry *reg;
	bWindow *wnd = bCreateWindow2(namespace, NULL, 0, w, h, NULL,
			&layer_wnd_ops);
	bLayerWindow *ww = g_malloc0(sizeof(bLayerWindow));
	if(!ww){
		g_critical("Failed to allocate memory for window");
		goto destroy;
	}
	wnd->private = ww;

	ww->disp = bGetWaylandDisplay(wnd->disp);
	if(!ww->disp){
		g_critical("Display is not a Wayland display");
		goto destroy;
	}

	ww->xkbctx = xkb_context_new(0);
	if(!ww->xkbctx){
		g_critical("Failed to create XKB context");
		goto destroy;
	}

	reg = wl_display_get_registry(ww->disp);
	wl_registry_add_listener(reg, &listeners, &ww->reg);
	wl_display_roundtrip(ww->disp);
	wl_registry_destroy(reg);
	if(!ww->reg.comp){
		g_critical("No wl_compositor interface found.");
		goto destroy;
	}
	if(!ww->reg.layer_shell){
		g_critical("No layer shell found.");
		goto destroy;
	}
	if(!ww->reg.seat){
		g_critical("No seat available.");
		goto destroy;
	}
	if(!ww->reg.shm){
		g_critical("No wl_shm found.");
		goto destroy;
	}
	if(!ww->reg.text_input){
		g_warning("No text input method.");
	}

	ww->keyboard = wl_seat_get_keyboard(ww->reg.seat);
	if(ww->keyboard){
		wl_keyboard_add_listener(ww->keyboard,
			&keyboard_listener, wnd);
	}

	ww->wlsurf = wl_compositor_create_surface(ww->reg.comp);
	if(!ww->wlsurf) goto destroy;

	ww->layersurf = zwlr_layer_shell_v1_get_layer_surface(
			ww->reg.layer_shell, ww->wlsurf,
			output, layer, namespace);
	if(!ww->layersurf) goto destroy;
	zwlr_layer_surface_v1_set_layer(ww->layersurf, layer);
	zwlr_layer_surface_v1_set_margin(ww->layersurf,
			margin_top, margin_right,
			margin_bottom, margin_left);
	zwlr_layer_surface_v1_set_anchor(ww->layersurf, anchor);
	zwlr_layer_surface_v1_set_exclusive_zone(ww->layersurf,
			exclusive_zone);
	zwlr_layer_surface_v1_set_keyboard_interactivity(ww->layersurf,
			keyboard_interactivity);
	zwlr_layer_surface_v1_add_listener(ww->layersurf,
			&layer_surface_listener, wnd);
	wl_surface_commit(ww->wlsurf);

	while(!(ww->configured) && wl_display_dispatch(ww->disp) >= 0);

	if(w > 0) wnd->width = w;
	else wnd->width += w;
	if(h > 0) wnd->height = h;
	else wnd->height += h;

	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,
			wnd->width);
	ww->shmfd = bsh_create_shm_file(stride * wnd->height);
	if(ww->shmfd < 0) goto destroy;
	ww->pixels = mmap(NULL, stride * wnd->height,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, ww->shmfd, 0);
	if(ww->pixels == MAP_FAILED){
		g_critical("mmap: %s", strerror(errno));
		ww->pixels = NULL;
		goto destroy;
	}
	ww->pool = wl_shm_create_pool(ww->reg.shm, ww->shmfd,
			stride * wnd->height);
	if(!ww->pool) goto destroy;
	ww->buffer = wl_shm_pool_create_buffer(ww->pool, 0,
			wnd->width, wnd->height, stride,
			WL_SHM_FORMAT_ARGB8888);
	if(!ww->buffer) goto destroy;
	ww->surf = cairo_image_surface_create_for_data(ww->pixels,
			CAIRO_FORMAT_ARGB32, wnd->width, wnd->height,
			stride);
	if(!ww->surf) goto destroy;
	wnd->renderer = cairo_create(ww->surf);
	if(!wnd->renderer) goto destroy;
	wl_surface_attach(ww->wlsurf, ww->buffer, 0, 0);
	wl_surface_damage(ww->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(ww->wlsurf);
	return wnd;
destroy:
	bDestroyWindow(wnd);
	return NULL;
}

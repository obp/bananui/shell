/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SHELL_H_
#define _SHELL_H_

#include <gio/gio.h>
#include <bananui/utils.h>
#include "dialer.h"
#include "monitor_manager.h"
#include "monitor/monitor.h"
#include "notification_manager.h"

typedef struct _BshShell
{
	int activated;
	GApplication *app;
	struct wl_display *disp;

	struct bsh_homescreen *homescreen;
	struct bsh_input_method *input_method;
	struct bsh_registry *registry;
	struct bsh_settings *settings;
	struct bsh_startup_tracker *startup_tracker;
	struct bsh_top_panel *top_panel;
	struct bsh_virtual_keyboard *virtual_keyboard;
	struct bsh_window_manager *window_manager;
	BshDialer *dialer;
	BshMonitor *primary_monitor;
	BshMonitorManager *monitor_manager;
	BshNotificationManager *notification_manager;
	GAppLaunchContext *launch_context;
	GDBusProxy *flip_manager;
} BshShell;

void bsh_shell_set_primary_monitor (BshShell *shell, BshMonitor *monitor);
void bsh_shell_wake (BshShell *shell);
void bsh_shell_sleep (BshShell *shell);
BshShell *bsh_shell_get_default (void);

#endif

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "dbus/wroomd-dbus.h"

G_BEGIN_DECLS

#define BSH_TYPE_CELLULAR_MANAGER bsh_cellular_manager_get_type()

G_DECLARE_FINAL_TYPE (BshCellularManager, bsh_cellular_manager, BSH, CELLULAR_MANAGER, GObject)

BshCellularManager *bsh_cellular_manager_get_default     (void);
GPtrArray          *bsh_cellular_manager_get_sim_slots   (BshCellularManager   *self);
const char         *bsh_cellular_manager_get_slot_name   (BshCellularManager   *self,
                                                          WroomdDBusWroomdSlot *slot);
gboolean            bsh_cellular_manager_is_modem_locked (BshCellularManager   *self);
void                bsh_cellular_manager_hangup_calls    (BshCellularManager   *self);
void                bsh_cellular_manager_accept_calls    (BshCellularManager   *self);

G_END_DECLS

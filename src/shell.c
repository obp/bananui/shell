/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-shell"

#define LIBFEEDBACK_USE_UNSTABLE_API

#include "shell.h"

#include <bananui/bananui.h>
#include <libcallaudio.h>
#include <libfeedback.h>
#include <stdio.h>
#include "bsh_wayland.h"
#include "cellular_manager.h"
#include "homescreen.h"
#include "input_method.h"
#include "launch_context.h"
#include "settings.h"
#include "top_panel.h"
#include "window_manager.h"

#define BANANUI_SHELL_APP_ID "de.abscue.obp.Bananui.Shell"

static gint app_command_line(GApplication *self, GVariantDict *options,
		BshShell *shell)
{
	return -1;
}

static void
notify_ready (BshShell *shell)
{
  BshWayland *wl = bsh_wayland_get_default ();
  struct phosh_private *pp = bsh_wayland_get_phosh_private (wl);

  /* notify compositor that we're ready */
 if (pp)
   phosh_private_set_shell_state (pp, PHOSH_PRIVATE_SHELL_STATE_UP);
}

static void
initialize_ui (BshShell *shell)
{
  if (shell->dialer)
    g_object_unref (shell->dialer);
  if (shell->top_panel)
    destroy_top_panel (shell->top_panel);
  if (shell->homescreen)
    destroy_homescreen (shell->homescreen);
  if (shell->settings)
    destroy_settings (shell->settings);

  shell->dialer = bsh_dialer_new (shell->primary_monitor->wl_output);
  if (!shell->dialer)
    g_error ("Failed to initialize dialer");
  shell->top_panel = create_top_panel (shell->primary_monitor->wl_output, 30);
  if (!shell->top_panel)
    g_error ("Failed to initialize top panel");
  shell->homescreen = create_homescreen (shell->primary_monitor->wl_output);
  if (!shell->homescreen)
    g_error ("Failed to initialize homescreen");
  shell->settings = create_settings (shell->primary_monitor->wl_output);
  if (!shell->settings)
    g_error ("Failed to initialize settings");
}

static void
find_new_primary_monitor (BshShell *shell)
{
  int i, num = bsh_monitor_manager_get_num_monitors (shell->monitor_manager);

  if (num == 0)
    g_error ("No monitors available!");
  else
    g_debug ("Found %d monitors", num);

  for (i = 0; i < num; i++) {
    BshMonitor *tmp = bsh_monitor_manager_get_monitor (shell->monitor_manager, i);
    if (tmp != shell->primary_monitor && bsh_monitor_is_builtin (tmp)) {
      g_debug ("Setting primary monitor (%s)", tmp->name);
      shell->primary_monitor = tmp;
      return;
    }
  }

  shell->primary_monitor = bsh_monitor_manager_get_monitor (shell->monitor_manager, 0);
  g_debug ("Falling back to non-builtin primary monitor (%s)",
           shell->primary_monitor->name);
}

static void
on_monitor_added (BshShell *shell, BshMonitor *monitor)
{
  g_debug ("Monitor added (%s)", monitor->name);
}

static void
on_monitor_removed (BshShell *shell, BshMonitor *monitor)
{
  if (shell->primary_monitor == monitor) {
    g_debug ("Primary monitor removed (%s)", monitor->name);
    find_new_primary_monitor (shell);
    initialize_ui (shell);
  } else {
    g_debug ("Monitor removed (%s)", monitor->name);
  }
}

void
bsh_shell_set_primary_monitor (BshShell *shell, BshMonitor *monitor)
{
  if (shell->primary_monitor != monitor) {
    g_debug ("Setting primary monitor (%s)", monitor->name);
    shell->primary_monitor = monitor;
    initialize_ui (shell);
  }
}

void
bsh_shell_wake (BshShell *shell)
{
  g_debug ("Resuming shell...");
  bsh_monitor_set_power_save_mode (shell->primary_monitor,
                                   BSH_MONITOR_POWER_SAVE_MODE_ON);
  wl_display_flush (shell->disp);
}

void
bsh_shell_sleep (BshShell *shell)
{
  g_debug ("Suspending shell...");
  bsh_monitor_set_power_save_mode (shell->primary_monitor,
                                   BSH_MONITOR_POWER_SAVE_MODE_OFF);
  wl_display_flush (shell->disp);
}

static void app_activate(GApplication *app, BshShell *shell)
{
	if(shell->activated){
		return;
	}
	shell->activated = 1;
	g_application_hold(app); /* Shell should run forever */

	shell->disp = bGetWaylandDisplay(bGetDefaultDisplay());
	if(!shell->disp)
		g_error("Failed to get wayland display");

	shell->monitor_manager = bsh_monitor_manager_new();
	if(!shell->monitor_manager)
		g_error("Failed to initialize monitor manager");

	g_signal_connect_swapped (shell->monitor_manager,
			"monitor-added",
			G_CALLBACK (on_monitor_added),
			shell);
	g_signal_connect_swapped (shell->monitor_manager,
			"monitor-removed",
			G_CALLBACK (on_monitor_removed),
			shell);
	bsh_wayland_roundtrip (bsh_wayland_get_default ());
	find_new_primary_monitor(shell);

	initialize_ui(shell);

	shell->launch_context = bsh_launch_context_new();
	if(!shell->launch_context)
		g_error("Failed to initialize launch context");
	shell->window_manager = create_window_manager();
	if(!shell->window_manager)
		g_error("Failed to initialize window manager");
	shell->input_method = create_input_method();
	if(!shell->input_method)
		g_error("Failed to initialize input method");
	shell->notification_manager = bsh_notification_manager_new();
	if(!shell->notification_manager)
		g_error("Failed to initialize notification manager");

	homescreen_show(shell->homescreen);
	notify_ready(shell);
}

static void
flip_state_changed_cb (GDBusProxy *proxy,
                       GVariant   *changed_props,
                       GVariant   *invalidated_props,
                       BshShell   *shell)
{
  g_autofree char *state = NULL;

  if (!g_variant_lookup (changed_props, "State", "s", &state))
    return;

  if (strcmp (state, "closed") == 0) {
    bsh_shell_sleep (shell);
    bsh_cellular_manager_hangup_calls (bsh_cellular_manager_get_default ());
  } else {
    bsh_shell_wake (shell);
    /*
     * We should actually do
     *
     * bsh_cellular_manager_accept_calls (bsh_cellular_manager_get_default ());
     *
     * here but there is currently no way to see who's calling on flip phones
     * so the user has to open the phone before deciding whether to answer.
     */
  }
}

static void
flip_manager_init_cb (GObject      *object,
                      GAsyncResult *res,
                      BshShell     *shell)
{
  g_autoptr(GError) err = NULL;

  shell->flip_manager = g_dbus_proxy_new_finish (res, &err);
  if (shell->flip_manager == NULL) {
    g_warning ("Could not connect to flip manager service: %s",
               err->message);
    return;
  }

  g_signal_connect (shell->flip_manager,
                    "g-properties-changed",
                    G_CALLBACK(flip_state_changed_cb),
                    shell);
}

static void
watch_flip_manager (BshShell *shell)
{
  g_dbus_proxy_new_for_bus (G_BUS_TYPE_SESSION,
                            G_DBUS_PROXY_FLAGS_NONE,
                            NULL,
                            "de.abscue.obp.Bananui.Power",
                            "/de/abscue/obp/Bananui/Switch/flip",
                            "de.abscue.obp.Bananui.Switch",
                            NULL,
                            (GAsyncReadyCallback)flip_manager_init_cb,
                            shell);
}

static BshShell *default_shell = NULL;

BshShell *
bsh_shell_get_default (void)
{
  return default_shell;
}

int main(int argc, char **argv)
{
	BshShell shell = {};

	default_shell = &shell;

	lfb_init ("de.abscue.obp.Bananui.Shell", NULL);
	call_audio_init (NULL);

	bDefaultTheme(bLoadScript("themes/shell.ipt"));

	watch_flip_manager(&shell);

	shell.app = g_application_new(BANANUI_SHELL_APP_ID,
			G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect(shell.app, "activate",
			G_CALLBACK(app_activate), &shell);
	g_signal_connect(shell.app, "handle-local-options",
			G_CALLBACK(app_command_line), &shell);

	return g_application_run(shell.app, argc, argv);
}

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _WINDOW_MANAGER_H_
#define _WINDOW_MANAGER_H_

#include <gio/gio.h>

struct bsh_window_manager;

int window_manager_open_app(struct bsh_window_manager *wm,
		GAppInfo *app);
void window_manager_go_next(struct bsh_window_manager *wm);
void window_manager_go_prev(struct bsh_window_manager *wm);
void window_manager_close(struct bsh_window_manager *wm);
gboolean window_manager_has_window(struct bsh_window_manager *wm);
struct bsh_window_manager *create_window_manager(void);

#endif

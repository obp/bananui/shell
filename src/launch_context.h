/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LAUNCH_CONTEXT_H_
#define _LAUNCH_CONTEXT_H_

#include <gio/gdesktopappinfo.h>

struct bsh_shell;

#define BSH_TYPE_LAUNCH_CONTEXT (bsh_launch_context_get_type())

G_DECLARE_FINAL_TYPE(BshLaunchContext, bsh_launch_context, BSH, LAUNCH_CONTEXT, GAppLaunchContext);

GAppLaunchContext *bsh_launch_context_new(void);

#endif

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "dbus/wroomd-dbus.h"

#include <bananui/bananui.h>

G_BEGIN_DECLS

#define BSH_TYPE_SLOT_WIDGET bsh_slot_widget_get_type()

G_DECLARE_FINAL_TYPE (BshSlotWidget, bsh_slot_widget, BSH, SLOT_WIDGET, GObject)

typedef enum {
  BSH_SLOT_WIDGET_FLAG_NONE      = 0,
  BSH_SLOT_WIDGET_FLAG_IS_BUTTON = 1 << 0,
} BshSlotWidgetFlags;

BshSlotWidget        *bsh_slot_widget_new           (WroomdDBusWroomdSlot *slot,
                                                     BshSlotWidgetFlags flags);
const char           *bsh_slot_widget_get_action    (BshSlotWidget *widget);
gboolean              bsh_slot_widget_is_selectable (BshSlotWidget *widget);
gboolean              bsh_slot_widget_is_unlocked   (BshSlotWidget *widget);
WroomdDBusWroomdSlot *bsh_slot_widget_get_slot      (BshSlotWidget *widget);
bBox                 *bsh_slot_widget_into_box      (BshSlotWidget *widget);

G_END_DECLS

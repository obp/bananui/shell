/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SETTINGS_BRIGHTNESS_H_
#define _SETTINGS_BRIGHTNESS_H_

#include <bananui/bananui.h>

struct bsh_setting_brightness {
  bBox       *box;
  bBox       *iconbox;
  bBox       *slider;
  bBox       *slider_fill;
  bBox       *slider_handle;
  bStyleSpec  slider_fill_style;
  bContent   *icon;
  bWindow   **wnd;
  GDBusProxy *brightness_proxy;
  gboolean    setting_brightness;
  int         brightness;
};

void brightness_decrease (struct bsh_setting_brightness *brightness);
void brightness_increase (struct bsh_setting_brightness *brightness);
void brightness_add (bWindow                      **wnd,
                     bBox                          *parent,
                     struct bsh_setting_brightness *brightness);

#endif

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * Based on https://gitlab.gnome.org/World/Phosh/phosh/-/blob/main/src/settings/brightness.c
 *
 * Copyright (C) 2018 Purism SPC
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "settings-brightness"
#include "brightness.h"


static void
brightness_changed_cb (GDBusProxy *proxy,
                       GVariant   *changed_props,
                       GVariant   *invalidated_props,
                       gpointer    user_data)
{
  struct bsh_setting_brightness *data = user_data;
  unsigned int value;
  gboolean ret;

  ret = g_variant_lookup (changed_props,
                          "Brightness",
                          "u", &value);
  g_return_if_fail (ret);
  if (value > 100)
    value = 100;
  data->slider_fill_style.items.min_width = value;
  if (*data->wnd) {
    bInvalidateWithChildren (data->box);
    bRequestFrame (*data->wnd);
  }
}


static void
brightness_init_cb (GObject                       *source_object,
                    GAsyncResult                  *res,
                    struct bsh_setting_brightness *data)
{
  g_autoptr(GError) err = NULL;
  GVariant *var;
  unsigned int value;

  data->brightness_proxy = g_dbus_proxy_new_finish (res, &err);
  if (data->brightness_proxy == NULL) {
    g_warning ("Could not connect to brightness service: %s",
               err->message);
    return;
  }

  /* Set scale to current brightness */
  var = g_dbus_proxy_get_cached_property (data->brightness_proxy,
                                          "Brightness");
  if (var) {
    g_variant_get (var, "u", &value);
    data->setting_brightness = TRUE;
    if (value > 100)
      value = 100;
    data->slider_fill_style.items.min_width = value;
    if (*data->wnd) {
      bInvalidateWithChildren (data->box);
      bRequestFrame (*data->wnd);
    }
    data->setting_brightness = FALSE;
    g_variant_unref (var);
  }

  g_signal_connect (data->brightness_proxy,
                    "g-properties-changed",
                    G_CALLBACK(brightness_changed_cb),
                    data);
}


static void
brightness_set_cb (GDBusProxy                    *proxy,
                   GAsyncResult                  *res,
                   struct bsh_setting_brightness *data)
{
  GError *err = NULL;
  GVariant *var;

  var = g_dbus_proxy_call_finish (proxy, res, &err);

  data->setting_brightness = FALSE;

  if (err) {
    g_warning ("Could not set brightness: %s", err->message);
    g_error_free (err);
    return;
  }

  if (var)
    g_variant_unref (var);
}


void
brightness_set (struct bsh_setting_brightness *data, int brightness)
{
  if (!data->brightness_proxy)
    return;

  if (data->setting_brightness)
    return;

  data->setting_brightness = TRUE;
  g_dbus_proxy_call (data->brightness_proxy,
                     "org.freedesktop.DBus.Properties.Set",
                     g_variant_new (
                         "(ssv)",
                         "de.abscue.obp.Bananui.Backlight",
                         "Brightness",
                         g_variant_new ("u", brightness)),
                     G_DBUS_CALL_FLAGS_NONE,
                     2000,
                     NULL,
                     (GAsyncReadyCallback)brightness_set_cb,
                     data);
}

void
brightness_decrease (struct bsh_setting_brightness *data)
{
  if (!data->brightness_proxy)
    return;

  if (data->setting_brightness)
    return;

  data->setting_brightness = TRUE;
  g_dbus_proxy_call (data->brightness_proxy,
                     "de.abscue.obp.Bananui.Backlight.Decrease",
		     NULL,
                     G_DBUS_CALL_FLAGS_NONE,
                     2000,
                     NULL,
                     (GAsyncReadyCallback)brightness_set_cb,
                     data);
}

void
brightness_increase (struct bsh_setting_brightness *data)
{
  if (!data->brightness_proxy)
    return;

  if (data->setting_brightness)
    return;

  data->setting_brightness = TRUE;
  g_dbus_proxy_call (data->brightness_proxy,
                     "de.abscue.obp.Bananui.Backlight.Increase",
		     NULL,
                     G_DBUS_CALL_FLAGS_NONE,
                     2000,
                     NULL,
                     (GAsyncReadyCallback)brightness_set_cb,
                     data);
}

void
brightness_add (bWindow                       **wnd,
                bBox                          *parent,
                struct bsh_setting_brightness *data)
{
  g_autoptr(GError) err = NULL;
  g_autoptr(GDBusConnection) session_con = NULL;
  GCancellable *cancel;

  data->wnd = wnd;
  data->brightness = 100;

  bAddBox (parent,
           data->box = bCreateBox("shell settings brightness"));
  bAddBox (data->box,
           data->iconbox = bCreateBox("shell settings brightness icon"));
  bAddContent (data->iconbox,
               data->icon = bCreateIcon("display-brightness-symbolic", 32));
  bAddBox (data->box,
           data->slider = bCreateBox("shell settings slider"));
  bAddBox (data->slider,
           data->slider_fill = bCreateBox("shell settings slider fill"));
  bAddBox (data->slider,
           data->slider_handle = bCreateBox("shell settings slider handle"));
  data->slider_fill_style.override_min_width = 1;
  data->slider_fill_style.items.min_width = data->brightness;
  data->slider_fill->custom_style = &data->slider_fill_style;

  session_con = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &err);
  if (err != NULL) {
    g_warning ("Cannot connect to session bus: %s", err->message);
    return;
  }

  cancel = g_cancellable_new ();
  g_dbus_proxy_new (session_con,
                    G_DBUS_PROXY_FLAGS_NONE,
                    NULL,
                    "de.abscue.obp.Bananui.Power",
                    "/de/abscue/obp/Bananui/Backlight/display",
                    "de.abscue.obp.Bananui.Backlight",
                    cancel,
                    (GAsyncReadyCallback)brightness_init_cb,
                    data);
}

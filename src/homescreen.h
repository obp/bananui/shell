/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _HOMESCREEN_H_
#define _HOMESCREEN_H_

struct bsh_shell;
struct bsh_homescreen;
struct wl_output;

void homescreen_show(struct bsh_homescreen *hs);
void homescreen_show_overview(struct bsh_homescreen *hs);
void homescreen_hide(struct bsh_homescreen *hs);
struct bsh_homescreen *create_homescreen(struct wl_output *output);
void destroy_homescreen(struct bsh_homescreen *hs);

#endif

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libcallaudio.h>
#include "call_counter.h"

static unsigned int call_count = 0;


static void
audio_cb (gboolean success, GError *error, gpointer data)
{
  if (error)
    g_warning ("Failed to set call audio mode: %s", error->message);
}


static void
call_mode_enable (void)
{
  if (call_audio_is_inited ())
    call_audio_select_mode_async (CALL_AUDIO_MODE_CALL, audio_cb, NULL);
}


static void
call_mode_disable (void)
{
  if (call_audio_is_inited ())
    call_audio_select_mode_async (CALL_AUDIO_MODE_DEFAULT, audio_cb, NULL);
}


void
callcounter_increase (void)
{
  call_count += 1;
  if (call_count > 0)
    call_mode_enable ();
}


void
callcounter_decrease (void)
{
  if (call_count > 0) {
    call_count -= 1;
    if (call_count == 0)
      call_mode_disable ();
  }
}


/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "dbus/wroomd-dbus.h"

G_BEGIN_DECLS

#define BSH_TYPE_SLOT_DIALOG bsh_slot_dialog_get_type()

G_DECLARE_FINAL_TYPE (BshSlotDialog, bsh_slot_dialog, BSH, SLOT_DIALOG, GObject)

struct wl_output;

typedef void (*BshSlotCallback)(BshSlotDialog *dlg, WroomdDBusWroomdSlot *slot, void *userdata);

BshSlotDialog *bsh_slot_dialog_new  (struct wl_output *output, gboolean select);
void           bsh_slot_dialog_show (BshSlotDialog *self);
void           bsh_slot_dialog_hide (BshSlotDialog *self);
void           bsh_slot_select      (struct wl_output *output,
                                     BshSlotCallback   cb,
                                     void             *userdata);
void           bsh_slot_dialog_show_if_needed (struct wl_output *output);

G_END_DECLS

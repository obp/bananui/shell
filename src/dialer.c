/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-dialer"

#include "dialer.h"

#include <bananui/bananui.h>
#include "slot_dialog.h"
#include "layerwindow.h"

enum {
  BSH_DIALER_PROP_0,
  BSH_DIALER_PROP_OUTPUT,
  BSH_DIALER_PROP_LAST_PROP,
};
static GParamSpec *props[BSH_DIALER_PROP_LAST_PROP];

struct _BshDialer {
  GObject parent;

  struct wl_output *output;
  bWindow *wnd;
  bCard *card;
  bContent *content;
  char *number;
  size_t number_size;
  gboolean was_star;
};

G_DEFINE_TYPE (BshDialer, bsh_dialer, G_TYPE_OBJECT)


static void
bsh_dialer_get_property (GObject *object,
                         guint property_id,
                         GValue *value,
                         GParamSpec *pspec)
{
  BshDialer *self = BSH_DIALER (object);

  switch (property_id) {
  case BSH_DIALER_PROP_OUTPUT:
    g_value_set_pointer (value, self->output);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_dialer_set_property (GObject *object,
                         guint property_id,
                         const GValue *value,
                         GParamSpec *pspec)
{
  BshDialer *self = BSH_DIALER (object);

  switch (property_id) {
  case BSH_DIALER_PROP_OUTPUT:
    self->output = g_value_get_pointer (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_dialer_dispose (GObject *object)
{
  BshDialer *self = BSH_DIALER (object);

  g_clear_pointer (&self->number, g_free);
  g_clear_pointer (&self->wnd, bDestroyWindow);
  g_clear_pointer (&self->card, bDestroyCard);

  G_OBJECT_CLASS (bsh_dialer_parent_class)->dispose (object);
}


static void
bsh_dialer_class_init (BshDialerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bsh_dialer_dispose;

  object_class->get_property = bsh_dialer_get_property;
  object_class->set_property = bsh_dialer_set_property;

  props[BSH_DIALER_PROP_OUTPUT] =
    g_param_spec_pointer ("output",
                          "Wayland Output",
                          "The wayland output to show the dialer on",
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, BSH_DIALER_PROP_LAST_PROP, props);
}


static void
bsh_dialer_init (BshDialer *self)
{
  bBox *box;
  self->number_size = 256;
  self->number = g_malloc(self->number_size);
  self->number[0] = '\0';
  self->card = bBuildCard (bDefaultTheme (NULL),
                           "shell dialer",
                           1, &box);
  if (box) {
    bAddContent (box, self->content = bCreateText(""));
    self->content->cursor = 0;
    self->content->cursor_visible = 1;
  }
}


BshDialer *
bsh_dialer_new (struct wl_output *output)
{
  return g_object_new (BSH_TYPE_DIALER,
                       "output", output,
                       NULL);
}


static int
dialer_handle_key (void *param, void *data)
{
  BshDialer *self = data;
  bKeyEvent *ev = param;

  bsh_dialer_key (self, ev->sym);

  return 0;
}


static int
dialer_render (void *param, void *data)
{
  BshDialer *self = data;

  if (self->card)
    bRenderCard (self->wnd, self->card);

  return 1;
}


void
bsh_dialer_show (BshDialer *self)
{
  if (self->wnd)
    return;

  self->wnd = bCreateLayerWindow ("bananui-shell dialer",
                                  self->output, 0, 0,
                                  ZWLR_LAYER_SHELL_V1_LAYER_TOP,
                                  -30, 0, 0, 0,
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
                                  0, 1);
  if (!self->wnd) {
    g_critical ("Failed to show dialer");
  } else {
    bRegisterEventHandler (&self->wnd->keydown, dialer_handle_key, self);
    bRegisterEventHandler (&self->wnd->redraw, dialer_render, self);
    bRequestFrame (self->wnd);
  }
}


void
bsh_dialer_hide (BshDialer *self)
{
  self->number[0] = '\0';
  self->content->cursor = 0;
  self->was_star = FALSE;
  bSetText (self->content, "");
  g_clear_pointer (&self->wnd, bDestroyWindow);
}


static void
call_dialed_cb (WroomdDBusWroomdSlot *slot,
                GAsyncResult         *res,
                char                 *number)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_slot_call_dial_finish (slot, res, &error)) {
    g_warning ("Error dialing call: %s", error->message);
  }
}


static void
place_call_with_slot (BshSlotDialog *dlg, WroomdDBusWroomdSlot *slot, void *data)
{
  char *number = data;
  wroomd_dbus_wroomd_slot_call_dial (slot,
                                     number,
                                     NULL,
                                     (GAsyncReadyCallback) call_dialed_cb,
                                     number);
}


static void
place_call (struct wl_output *output, const char *number)
{
  bsh_slot_select (output, place_call_with_slot, g_strdup(number));
}


void
bsh_dialer_key (BshDialer *self, xkb_keysym_t key)
{
  char ch;

  if (key == BANANUI_KEY_Star && self->was_star) {
    ch = self->number[self->content->cursor - 1];
    self->number[self->content->cursor - 1] = ch == '*' ? '+' : '*';
  } else if (key == BANANUI_KEY_Back) {
    unsigned int i;
    if (self->content->cursor <= 1) {
      bsh_dialer_hide (self);
      return;
    }
    for (i = self->content->cursor; self->number[i]; i++) {
      self->number[i - 1] = self->number[i];
    }
    self->number[i - 1] = '\0';
    self->content->cursor--;
  } else if (key == XKB_KEY_Return) {
    place_call (self->output, self->number);
    bsh_dialer_hide (self);
    return;
  } else {
    switch (key) {
    case XKB_KEY_1:         ch = '1'; break;
    case XKB_KEY_2:         ch = '2'; break;
    case XKB_KEY_3:         ch = '3'; break;
    case XKB_KEY_4:         ch = '4'; break;
    case XKB_KEY_5:         ch = '5'; break;
    case XKB_KEY_6:         ch = '6'; break;
    case XKB_KEY_7:         ch = '7'; break;
    case XKB_KEY_8:         ch = '8'; break;
    case XKB_KEY_9:         ch = '9'; break;
    case XKB_KEY_0:         ch = '0'; break;
    case BANANUI_KEY_Pound: ch = '#'; break;
    case BANANUI_KEY_Star:  ch = '*'; break;
    default:                return;
    }

    if (self->content->cursor + 1 >= self->number_size) {
      self->number_size *= 2;
      self->number = g_realloc (self->number, self->number_size);
    }

    self->number[self->content->cursor++] = ch;
    self->number[self->content->cursor] = '\0';
  }

  bSetText (self->content, self->number);
  bRequestFrame (self->wnd);

  self->was_star = key == BANANUI_KEY_Star;
}

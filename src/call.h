/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <bananui/keys.h>

#include "dbus/wroomd-dbus.h"

G_BEGIN_DECLS

#define BSH_TYPE_CALL bsh_call_get_type()

G_DECLARE_FINAL_TYPE (BshCall, bsh_call, BSH, CALL, GObject)

BshCall *bsh_call_new    (WroomdDBusWroomdCall *call);
void     bsh_call_accept (BshCall *call);
void     bsh_call_hangup (BshCall *call);

G_END_DECLS

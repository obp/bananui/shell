/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _MONITOR_MANAGER_H_
#define _MONITOR_MANAGER_H_

#include "bsh-display-dbus.h"
#include "monitor/monitor.h"

/**
 * BshMonitorManagerConfigMethod:
 * @BSH_MONITOR_MANAGER_CONFIG_METHOD_VERIFY: verify the configuration
 * @BSH_MONITOR_MANAGER_CONFIG_METHOD_TEMPORARY: configuration is temporary
 * @BSH_MONITOR_MANAGER_CONFIG_METHOD_PERSISTENT: configuration is permanent
 *
 * Equivalent to the 'method' enum in org.gnome.Mutter.DisplayConfig
 */
typedef enum _MetaMonitorsConfigMethod
{
  BSH_MONITOR_MANAGER_CONFIG_METHOD_VERIFY = 0,
  BSH_MONITOR_MANAGER_CONFIG_METHOD_TEMPORARY = 1,
  BSH_MONITOR_MANAGER_CONFIG_METHOD_PERSISTENT = 2,
} BshMonitorManagerConfigMethod;

#define BSH_TYPE_MONITOR_MANAGER (bsh_monitor_manager_get_type ())

G_DECLARE_FINAL_TYPE(BshMonitorManager,
                     bsh_monitor_manager,
                     BSH,
                     MONITOR_MANAGER,
                     BshDBusDisplayConfigSkeleton);

BshMonitorManager *bsh_monitor_manager_new                       (void);
BshMonitor        *bsh_monitor_manager_get_monitor               (BshMonitorManager *self,
                                                                  guint                num);
guint              bsh_monitor_manager_get_num_monitors          (BshMonitorManager *self);
BshMonitor        *bsh_monitor_manager_find_monitor              (BshMonitorManager *self,
                                                                  const char          *name);
void               bsh_monitor_manager_set_monitor_transform     (BshMonitorManager *self,
                                                                  BshMonitor        *monitor,
                                                                  BshMonitorTransform transform);
void               bsh_monitor_manager_apply_monitor_config      (BshMonitorManager *self);
gboolean           bsh_monitor_manager_enable_fallback           (BshMonitorManager *self);
void               bsh_monitor_manager_set_power_save_mode       (BshMonitorManager *self,
                                                                  BshMonitorPowerSaveMode mode);

#endif

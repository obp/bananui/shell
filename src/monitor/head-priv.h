/*
 * Based on src/monitor/head-priv.h from phosh
 *
 * Copyright (C) 2019-2022 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "monitor.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define BSH_TYPE_HEAD                 (bsh_head_get_type ())

G_DECLARE_FINAL_TYPE (BshHead, bsh_head, BSH, HEAD, GObject)

typedef struct _BshHead BshHead;

typedef struct _BshHeadMode {
  /*< private >*/
  struct zwlr_output_mode_v1 *wlr_mode;
  BshHead                    *head;

  int32_t                     width, height;
  int32_t                     refresh;
  gboolean                    preferred;
  char                       *name;
} BshHeadMode;

struct _BshHead {
  GObject                     parent;

  gchar                      *name;
  gchar                      *description;
  gchar                      *vendor, *product, *serial;
  gboolean                    enabled;

  struct {
    int32_t width, height;
  } phys;
  int32_t                     x, y;

  enum wl_output_transform    transform;
  double                      scale;
  BshHeadMode                *mode;
  GPtrArray                  *modes;

  struct pending {
    int32_t x, y;
    enum wl_output_transform transform;
    BshHeadMode *mode;
    double scale;
    gboolean enabled;
    gboolean seen;
  } pending;

  BshMonitorConnectorType conn_type;
  struct zwlr_output_head_v1 *wlr_head;
};


BshHead                    *bsh_head_new_from_wlr_head (gpointer wlr_head);
struct zwlr_output_head_v1 *bsh_head_get_wlr_head (BshHead *self);
gboolean                    bsh_head_get_enabled (BshHead *self);
void                        bsh_head_set_pending_enabled (BshHead *self, gboolean enabled);
BshHeadMode                *bsh_head_get_preferred_mode (BshHead *self);
gboolean                    bsh_head_is_builtin (BshHead *self);
BshHeadMode                *bsh_head_find_mode_by_name (BshHead *self, const char *name);
float *                     bsh_head_calculate_supported_mode_scales (BshHead     *head,
                                                                      BshHeadMode *mode,
                                                                      int           *n,
                                                                      gboolean       fractional);
void                        bsh_head_clear_pending (BshHead *self);
void                        bsh_head_set_pending_transform (BshHead             *self,
                                                            BshMonitorTransform  transform,
                                                            GPtrArray             *heads);

G_END_DECLS

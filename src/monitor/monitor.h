/*
 * Based on src/monitor/monitor.h from phosh
 *
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "bsh_wayland.h"
#include "enums.h"

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * BshMonitorConnectorType:
 * @BSH_MONITOR_CONNECTOR_TYPE_Unknown: unknown connector type
 * @BSH_MONITOR_CONNECTOR_TYPE_VGA: a VGA connector
 * @BSH_MONITOR_CONNECTOR_TYPE_DVII: a DVII connector
 * @BSH_MONITOR_CONNECTOR_TYPE_DVID: a DVID connector
 * @BSH_MONITOR_CONNECTOR_TYPE_DVIA: a DVIA connector
 * @BSH_MONITOR_CONNECTOR_TYPE_Composite: a Composite connector
 * @BSH_MONITOR_CONNECTOR_TYPE_SVIDEO: a SVIDEO connector
 * @BSH_MONITOR_CONNECTOR_TYPE_LVDS: a LVDS connector
 * @BSH_MONITOR_CONNECTOR_TYPE_Component: a Component connector
 * @BSH_MONITOR_CONNECTOR_TYPE_9PinDIN: a 9PinDIN connector
 * @BSH_MONITOR_CONNECTOR_TYPE_DisplayPort: a DisplayPort connector
 * @BSH_MONITOR_CONNECTOR_TYPE_HDMIA: a HDMIA connector
 * @BSH_MONITOR_CONNECTOR_TYPE_HDMIB: a HDMIB connector
 * @BSH_MONITOR_CONNECTOR_TYPE_TV: a TV connector
 * @BSH_MONITOR_CONNECTOR_TYPE_eDP: a eDP connector
 * @BSH_MONITOR_CONNECTOR_TYPE_VIRTUAL: a Virtual connector
 * @BSH_MONITOR_CONNECTOR_TYPE_DSI: a DSI connector
 *
 * This matches the values in drm_mode.h
 */
typedef enum _BshMonitorConnectorType
{
  BSH_MONITOR_CONNECTOR_TYPE_Unknown = 0,
  BSH_MONITOR_CONNECTOR_TYPE_VGA = 1,
  BSH_MONITOR_CONNECTOR_TYPE_DVII = 2,
  BSH_MONITOR_CONNECTOR_TYPE_DVID = 3,
  BSH_MONITOR_CONNECTOR_TYPE_DVIA = 4,
  BSH_MONITOR_CONNECTOR_TYPE_Composite = 5,
  BSH_MONITOR_CONNECTOR_TYPE_SVIDEO = 6,
  BSH_MONITOR_CONNECTOR_TYPE_LVDS = 7,
  BSH_MONITOR_CONNECTOR_TYPE_Component = 8,
  BSH_MONITOR_CONNECTOR_TYPE_9PinDIN = 9,
  BSH_MONITOR_CONNECTOR_TYPE_DisplayPort = 10,
  BSH_MONITOR_CONNECTOR_TYPE_HDMIA = 11,
  BSH_MONITOR_CONNECTOR_TYPE_HDMIB = 12,
  BSH_MONITOR_CONNECTOR_TYPE_TV = 13,
  BSH_MONITOR_CONNECTOR_TYPE_eDP = 14,
  BSH_MONITOR_CONNECTOR_TYPE_VIRTUAL = 15,
  BSH_MONITOR_CONNECTOR_TYPE_DSI = 16,
} BshMonitorConnectorType;

/**
 * BshMonitorTransform:
 * @BSH_MONITOR_TRANSFORM_NORMAL: normal
 * @BSH_MONITOR_TRANSFORM_90: 90 degree clockwise
 * @BSH_MONITOR_TRANSFORM_180: 180 degree clockwise
 * @BSH_MONITOR_TRANSFORM_270: 270 degree clockwise
 * @BSH_MONITOR_TRANSFORM_FLIPPED: flipped clockwise
 * @BSH_MONITOR_TRANSFORM_FLIPPED_90: flipped and 90 deg
 * @BSH_MONITOR_TRANSFORM_FLIPPED_180: flipped and 180 deg
 * @BSH_MONITOR_TRANSFORM_FLIPPED_270: flipped and 270 deg
 *
 * the monitors rotation. This corresponds to the values in
 * the org.gnome.Mutter.DisplayConfig DBus protocol.
 */
typedef enum _BshMonitorTransform
{
  BSH_MONITOR_TRANSFORM_NORMAL,
  BSH_MONITOR_TRANSFORM_90,
  BSH_MONITOR_TRANSFORM_180,
  BSH_MONITOR_TRANSFORM_270,
  BSH_MONITOR_TRANSFORM_FLIPPED,
  BSH_MONITOR_TRANSFORM_FLIPPED_90,
  BSH_MONITOR_TRANSFORM_FLIPPED_180,
  BSH_MONITOR_TRANSFORM_FLIPPED_270,
} BshMonitorTransform;


typedef struct _BshMonitorMode
{
  int width, height;
  int refresh;
  guint32 flags;
} BshMonitorMode;

/**
 * BshMonitorPowerSaveMode:
 * @BSH_MONITOR_POWER_SAVE_MODE_ON: The monitor is on
 * @BSH_MONITOR_POWER_SAVE_MODE_OFF: The monitor is off (saving power)
 *
 * The power save mode of a monitor
 */
typedef enum _BshMonitorPowerSaveMode {
  BSH_MONITOR_POWER_SAVE_MODE_OFF = 0,
  BSH_MONITOR_POWER_SAVE_MODE_ON  = 1,
} BshMonitorPowerSaveMode;

#define BSH_TYPE_MONITOR                 (bsh_monitor_get_type ())

struct _BshMonitor {
  GObject parent;

  struct wl_output *wl_output;
  struct zxdg_output_v1 *xdg_output;
  struct zwlr_output_power_v1 *wlr_output_power;
  BshMonitorPowerSaveMode power_mode;

  int x, y, width, height;
  int subpixel;
  gint32 transform;

  struct {
    gint32 x, y, width, height;
  } logical;

  int width_mm;
  int height_mm;

  char *vendor;
  char *product;
  char *description;

  GArray *modes;
  guint current_mode;
  guint preferred_mode;

  char *name;
  BshMonitorConnectorType conn_type;

  gboolean wl_output_done;
  gboolean xdg_output_done;

  struct zwlr_gamma_control_v1 *gamma_control;
  guint32 n_gamma_entries;
};

G_DECLARE_FINAL_TYPE (BshMonitor, bsh_monitor, BSH, MONITOR, GObject)

BshMonitor        *bsh_monitor_new_from_wl_output (gpointer wl_output);
BshMonitorMode    *bsh_monitor_get_current_mode (BshMonitor *self);
gboolean           bsh_monitor_is_configured (BshMonitor *self);
gboolean           bsh_monitor_is_builtin (BshMonitor *self);
gboolean           bsh_monitor_is_flipped (BshMonitor *self);
gboolean           bsh_monitor_has_gamma (BshMonitor *self);
guint              bsh_monitor_get_transform (BshMonitor *self);
void               bsh_monitor_set_power_save_mode (BshMonitor *self,
                                                    BshMonitorPowerSaveMode mode);
BshMonitorPowerSaveMode bsh_monitor_get_power_save_mode (BshMonitor *self);
BshMonitorConnectorType bsh_monitor_connector_type_from_name (const char *name);
gboolean           bsh_monitor_connector_is_builtin (BshMonitorConnectorType type);
struct wl_output  *bsh_monitor_get_wl_output (BshMonitor *self);
float              bsh_monitor_get_fractional_scale (BshMonitor *self);

gboolean           bsh_monitor_transform_is_tilted (BshMonitorTransform transform);

G_END_DECLS

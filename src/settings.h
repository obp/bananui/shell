/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SETTINGS_H_
#define _SETTINGS_H_

struct wl_output;
struct bsh_shell;
struct bsh_settings;

void settings_show(struct bsh_settings *settings);
void settings_hide_now(struct bsh_settings *settings);
void settings_hide(struct bsh_settings *settings);
struct bsh_settings *create_settings(struct wl_output *output);
void destroy_settings(struct bsh_settings *settings);

#endif

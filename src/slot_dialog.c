/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-slot-dialog"

#include "slot_dialog.h"

#include <bananui/bananui.h>
#include "cellular_manager.h"
#include "slot_widget.h"
#include "layerwindow.h"

enum {
  BSH_SLOT_DIALOG_PROP_0,
  BSH_SLOT_DIALOG_PROP_OUTPUT,
  BSH_SLOT_DIALOG_PROP_SELECT,
  BSH_SLOT_DIALOG_PROP_LAST_PROP,
};
static GParamSpec *props[BSH_SLOT_DIALOG_PROP_LAST_PROP];

enum {
  SIGNAL_SELECTED,
  N_SIGNALS,
};
static unsigned int signals[N_SIGNALS];

struct _BshSlotDialog {
  GObject parent;

  struct wl_output *output;
  gboolean          select;
  gboolean          no_slots;
  bWindow          *wnd;
  bCard            *card;
  bSoftkeyPanel    *sk;
  bBox             *box;
};

G_DEFINE_TYPE (BshSlotDialog, bsh_slot_dialog, G_TYPE_OBJECT)


static void
bsh_slot_dialog_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  BshSlotDialog *self = BSH_SLOT_DIALOG (object);

  switch (property_id) {
  case BSH_SLOT_DIALOG_PROP_OUTPUT:
    g_value_set_pointer (value, self->output);
    break;
  case BSH_SLOT_DIALOG_PROP_SELECT:
    g_value_set_boolean (value, self->select);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_slot_dialog_set_property (GObject *object,
                              guint property_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  BshSlotDialog *self = BSH_SLOT_DIALOG (object);

  switch (property_id) {
  case BSH_SLOT_DIALOG_PROP_OUTPUT:
    self->output = g_value_get_pointer (value);
    break;
  case BSH_SLOT_DIALOG_PROP_SELECT:
    self->select = g_value_get_boolean (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_slot_dialog_widget_updated_cb (BshSlotDialog *self,
                                   BshSlotWidget *widget)
{
  if (self->wnd) {
    const char *action = bsh_slot_widget_get_action (widget);
    if (action) { /* widget is currently selected */
      gboolean is_clickable = self->select ?
        bsh_slot_widget_is_selectable (widget) :
        bsh_slot_widget_is_unlocked (widget);
      bSetSoftkeyText (self->sk, NULL, is_clickable ? "OK" : "", action);
      bInvalidateWithChildren(self->card->box);
      bRequestFrame (self->wnd);
    }
  }
}


static void
bsh_slot_dialog_widget_clicked_cb (BshSlotDialog *self,
                                   BshSlotWidget *widget)
{
  gboolean is_clickable = self->select ?
    bsh_slot_widget_is_selectable (widget) :
    bsh_slot_widget_is_unlocked (widget);

  if (!is_clickable)
    return;

  if (self->select) {
    g_signal_emit (G_OBJECT(self),
                   signals[SIGNAL_SELECTED],
                   0,
                   bsh_slot_widget_get_slot (widget));
  }

  bsh_slot_dialog_hide (self);
}


static void
bsh_slot_dialog_updated_slots_cb (BshCellularManager *cm,
                                  GParamSpec         *pspec,
                                  BshSlotDialog      *self)
{
  g_autoptr(GPtrArray) slots = bsh_cellular_manager_get_sim_slots (cm);
  unsigned int         i;
  g_debug ("Slots updated");

  if (!self->box)
    return;

  bDestroyBoxChildren (self->box);

  for (i = 0; i < slots->len; i++) {
    WroomdDBusWroomdSlot *slot = g_ptr_array_index (slots, i);
    BshSlotWidget        *widget =
      bsh_slot_widget_new (slot, BSH_SLOT_WIDGET_FLAG_IS_BUTTON);

    g_signal_connect_swapped (widget,
                              "updated",
                              G_CALLBACK(bsh_slot_dialog_widget_updated_cb),
                              self);

    g_signal_connect_swapped (widget,
                              "clicked",
                              G_CALLBACK(bsh_slot_dialog_widget_clicked_cb),
                              self);

    bAddBox (self->box, bsh_slot_widget_into_box (widget));
  }

  if (i == 0) {
    bAddContent (self->box, bCreateText ("No slots"));
    bSetSoftkeyText (self->sk, "", "OK", "");
    self->no_slots = TRUE;
  } else {
    self->no_slots = FALSE;
  }

  if (self->wnd)
    bRequestFrame (self->wnd);
}


static void
bsh_slot_dialog_constructed (GObject *object)
{
  BshSlotDialog *self = BSH_SLOT_DIALOG (object);
  BshCellularManager *cm = bsh_cellular_manager_get_default ();

  G_OBJECT_CLASS (bsh_slot_dialog_parent_class)->constructed (object);

  g_signal_connect_object (cm,
                           "notify::slots",
                           G_CALLBACK(bsh_slot_dialog_updated_slots_cb),
                           self,
                           G_CONNECT_DEFAULT);

  bsh_slot_dialog_updated_slots_cb (cm, NULL, self);
}


static void
bsh_slot_dialog_dispose (GObject *object)
{
  BshSlotDialog *self = BSH_SLOT_DIALOG (object);

  bDestroyCard (self->card);
  bDestroySoftkeyPanel (self->sk);

  G_OBJECT_CLASS (bsh_slot_dialog_parent_class)->dispose (object);
}


static void
bsh_slot_dialog_class_init (BshSlotDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bsh_slot_dialog_constructed;
  object_class->dispose = bsh_slot_dialog_dispose;

  object_class->get_property = bsh_slot_dialog_get_property;
  object_class->set_property = bsh_slot_dialog_set_property;

  props[BSH_SLOT_DIALOG_PROP_OUTPUT] =
    g_param_spec_pointer ("output",
                          "Wayland Output",
                          "The wayland output to show the slot dialog on",
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  props[BSH_SLOT_DIALOG_PROP_SELECT] =
    g_param_spec_boolean ("select",
                          "Select slot",
                          "Indicates that the dialog is for selecting a slot",
                          FALSE,
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  signals[SIGNAL_SELECTED] =
    g_signal_new ("selected",
                  BSH_TYPE_SLOT_DIALOG,
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  1,
                  WROOMD_DBUS_TYPE_WROOMD_SLOT);

  g_object_class_install_properties (object_class, BSH_SLOT_DIALOG_PROP_LAST_PROP, props);
}


static void
bsh_slot_dialog_init (BshSlotDialog *self)
{
  self->card = bBuildCard (bDefaultTheme (NULL),
                           "shell slot dialog",
                           1, &self->box);
  self->sk = bCreateSoftkeyPanel ();
}


BshSlotDialog *
bsh_slot_dialog_new (struct wl_output *output, gboolean select)
{
  return g_object_new (BSH_TYPE_SLOT_DIALOG,
                       "output", output,
                       "select", select,
                       NULL);
}


static int
slot_dialog_handle_key (void *param, void *data)
{
  BshSlotDialog *self = data;
  bKeyEvent *ev = param;

  if (ev->sym == BANANUI_KEY_Back ||
      ev->sym == BANANUI_KEY_EndCall ||
      (ev->sym == XKB_KEY_Return && self->no_slots))
    bsh_slot_dialog_hide (self);

  return 0;
}


static int
slot_dialog_render (void *param, void *data)
{
  BshSlotDialog *self = data;

  bRenderCard (self->wnd, self->card);
  bRenderCard (self->wnd, self->sk->card);

  return 1;
}


void
bsh_slot_dialog_show (BshSlotDialog *self)
{
  if (self->wnd)
    return;

  self->wnd = bCreateLayerWindow ("bananui-shell slot dialog",
                                  self->output, 0, 0,
                                  ZWLR_LAYER_SHELL_V1_LAYER_TOP,
                                  -30, 0, 0, 0,
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
                                  0, 1);
  if (!self->wnd) {
    g_critical ("Failed to show slot dialog");
  } else {
    bRegisterEventHandler (&self->wnd->keydown,
                           slot_dialog_handle_key,
                           g_object_ref (self));
    bRegisterEventHandler (&self->wnd->redraw, slot_dialog_render, self);
    bFocusCard (self->wnd, self->card);
    bRequestFrame (self->wnd);
  }
}


void
bsh_slot_dialog_hide (BshSlotDialog *self)
{
  g_clear_pointer (&self->wnd, bDestroyWindow);
  /* The window holds a reference for the keydown handler. Release it here. */
  g_object_unref (self);
}


void
bsh_slot_select (struct wl_output *output,
                 BshSlotCallback   cb,
                 void             *userdata)
{
  BshSlotDialog *dlg = bsh_slot_dialog_new (output, TRUE);
  g_signal_connect (dlg, "selected", (GCallback) cb, userdata);
  bsh_slot_dialog_show (dlg);
  g_object_unref (dlg);
}


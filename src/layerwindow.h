/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LAYERWINDOW_H_
#define _LAYERWINDOW_H_

#include "wlr-layer-shell-unstable-v1-client-protocol.h"

bWindow *bCreateLayerWindow(const char *namespace,
		struct wl_output *output,
		int w, int h, int layer,
		int margin_top, int margin_left,
		int margin_bottom, int margin_right,
		unsigned int anchor, int exclusive_zone,
		unsigned int keyboard_interactivity);

#endif

/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _TOP_PANEL_H_
#define _TOP_PANEL_H_

struct wl_output;
struct bsh_shell;
struct bsh_top_panel;

void unset_top_panel_color(struct bsh_top_panel *p);
void set_top_panel_color(struct bsh_top_panel *p,
		double r, double g, double b, double a,
		double cr, double cg, double cb, double ca);
void set_top_panel_input_method_status(struct bsh_top_panel *p, const char *status);
struct bsh_top_panel *create_top_panel(struct wl_output *output, int height);
void destroy_top_panel(struct bsh_top_panel *p);

#endif

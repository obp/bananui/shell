/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-launch-context"

#include "bsh_wayland.h"
#include "shell.h"
#include "homescreen.h"
#include "launch_context.h"

typedef struct _BshLaunchContext {
	GAppLaunchContext parent;
	struct phosh_private_startup_tracker *startup_tracker;
} BshLaunchContext;

G_DEFINE_TYPE(BshLaunchContext, bsh_launch_context, G_TYPE_APP_LAUNCH_CONTEXT)

GAppLaunchContext *bsh_launch_context_new(void)
{
	return g_object_new(BSH_TYPE_LAUNCH_CONTEXT, NULL);
}

static void handle_startup_id(void *data,
		struct phosh_private_startup_tracker *startup_tracker,
		const char *startup_id,
		uint32_t protocol,
		uint32_t flags)
{
	g_debug("Compositor reported startup ID: %s", startup_id);
}

static void handle_launched(void *data,
		struct phosh_private_startup_tracker *startup_tracker,
		const char *startup_id,
		uint32_t protocol,
		uint32_t flags)
{
	g_debug("Compositor reported launch: %s", startup_id);
}

static const struct phosh_private_startup_tracker_listener
startup_tracker_listener = {
	.startup_id = handle_startup_id,
	.launched = handle_launched,
};

static void init_startup_tracker(BshLaunchContext *self)
{
	struct phosh_private *pp = bsh_wayland_get_phosh_private(bsh_wayland_get_default());
	if(!pp) return;
	self->startup_tracker = phosh_private_get_startup_tracker(pp);
	phosh_private_startup_tracker_add_listener(self->startup_tracker,
			&startup_tracker_listener, self);
}

static void bsh_launch_context_constructed(GObject *object)
{
	BshLaunchContext *self = BSH_LAUNCH_CONTEXT(object);
	init_startup_tracker(self);
}

static char *bsh_launch_context_get_display(GAppLaunchContext *context,
		GAppInfo *app,
		GList *files)
{
	return NULL;
}

static char *bsh_launch_context_get_startup_notify_id(GAppLaunchContext *context,
		GAppInfo *app,
		GList *files)
{
	g_debug("startup_notify_id requested");
	return NULL;
}

static void bsh_launch_context_launch_failed(GAppLaunchContext *context,
		const char *startup_notify_id)
{
	BshShell *shell = bsh_shell_get_default();

	g_debug("Launch failed: %s", startup_notify_id);
	homescreen_show(shell->homescreen);
}

static void bsh_launch_context_launched(GAppLaunchContext *context,
		GAppInfo *app, GVariant *platform_data)
{
	g_debug("Launched %s", g_app_info_get_name(app));
}

static void bsh_launch_context_launch_started(GAppLaunchContext *context,
		GAppInfo *app, GVariant *platform_data)
{
	g_debug("Launching %s", g_app_info_get_name(app));
}

static void bsh_launch_context_class_init(BshLaunchContextClass *cls)
{
	GObjectClass *object_class = G_OBJECT_CLASS(cls);
	GAppLaunchContextClass *lc_class = G_APP_LAUNCH_CONTEXT_CLASS(cls);

	object_class->constructed = bsh_launch_context_constructed;

	lc_class->get_display = bsh_launch_context_get_display;
	lc_class->get_startup_notify_id = bsh_launch_context_get_startup_notify_id;
	lc_class->launch_failed = bsh_launch_context_launch_failed;
	lc_class->launched = bsh_launch_context_launched;
	lc_class->launch_started = bsh_launch_context_launch_started;
}

static void bsh_launch_context_init(BshLaunchContext *self)
{
}

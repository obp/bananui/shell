/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-input-method"

#include <glib.h>
#include <stdbool.h>
#include <linux/input-event-codes.h>
#include "bsh_wayland.h"
#include "shell.h"
#include "settings.h"
#include "top_panel.h"
#include "homescreen.h"
#include "input_method.h"

struct bsh_input_method {
	struct zwp_input_method_v2 *input_method;
	struct zwp_input_method_keyboard_grab_v2 *grab;
	struct zwp_virtual_keyboard_v1 *virtual_keyboard;
	struct zwlr_virtual_pointer_v1 *virtual_pointer;
	const char *current_chars, *current_char;
	uint32_t serial;
	uint32_t current_state, next_state;
	uint32_t move_timestamp;
	unsigned int move_timeout;
	int move_x, move_y;
	GHashTable *keys;
};

#define RELEASE_KEY (1 << 31)

#define IM_FLAG(c) ((c >= 'a' && c <= 'z') ? 1 << (c - 'a') : 1 << (c - '0'))
#define IM_FLAG_AUTOCAPITALIZE IM_FLAG('a')
#define IM_FLAG_CAPITALIZE IM_FLAG('c')
#define IM_FLAG_DISABLED IM_FLAG('d')
#define IM_FLAG_NUMERIC IM_FLAG('n')
#define IM_FLAG_EDITING IM_FLAG('t')
#define IM_FLAG_PENDING_AFFECTS_CASE IM_FLAG('p')
#define IM_FLAG_UPPERCASE IM_FLAG('u')
#define IM_FLAG_VIRTUAL_POINTER IM_FLAG('v')

static bool want_autolowercase(gunichar uch)
{
	return g_unichar_isalnum(uch);
}

static bool want_autouppercase(gunichar uch)
{
	return uch == g_utf8_get_char("!") ||
		uch == g_utf8_get_char("?") ||
		uch == g_utf8_get_char(".") ||
		uch == g_utf8_get_char("\n");
}

static void im_add_keymap_key(struct bsh_input_method *im,
		uint32_t keycode, const char *action)
{
	g_hash_table_insert(im->keys, GINT_TO_POINTER(keycode),
			(gpointer) action);
}

static void im_load_keymap(struct bsh_input_method *im)
{
	if(im->keys) g_hash_table_unref(im->keys);
	im->keys = g_hash_table_new(g_direct_hash, g_direct_equal);

	im_add_keymap_key(im, KEY_1, "d;\ntz:()$%{|}[\\]+~#`\ntn_1\nt:.,!?1/_-*&@:=<>\"';");
	im_add_keymap_key(im, KEY_2, "d;\ntn_2\ntu:ABC2\nt:abc2");
	im_add_keymap_key(im, KEY_3, "d;\ntn_3\ntu:DEF3\nt:def3");
	im_add_keymap_key(im, KEY_4, "d;\ntn_4\ntu:GHI4\nt:ghi4");
	im_add_keymap_key(im, KEY_5, "d;\nz^v.\ntn_5\ntu:JKL5\nt:jkl5");
	im_add_keymap_key(im, KEY_6, "d;\ntn_6\ntu:MNO6\nt:mno6");
	im_add_keymap_key(im, KEY_7, "d;\ntn_7\ntu:PQRS7\nt:pqrs7");
	im_add_keymap_key(im, KEY_8, "d;\ntn_8\ntu:TUV8\nt:tuv8");
	im_add_keymap_key(im, KEY_9, "d;\ntn_9\ntu:WXYZ9\nt:wxyz9");
	im_add_keymap_key(im, KEY_0, "d:\ntn_0\nt: 0");

	/* Cycle: autocapitalize -> uppercase -> numeric -> lowercase */
	im_add_keymap_key(im, KEY_NUMERIC_POUND, "td-d;\nta-a+u;\ntu-u+n;\ntn-n;\nt+a;");

	/* Shortcuts */
	im_add_keymap_key(im, KEY_NUMERIC_STAR, "+z");
	im_add_keymap_key(im, KEY_NUMERIC_STAR | RELEASE_KEY, "-z");
	im_add_keymap_key(im, KEY_UP, "z/s.\nvY-.");
	im_add_keymap_key(im, KEY_UP | RELEASE_KEY, "vY0.");
	im_add_keymap_key(im, KEY_DOWN, "z/h.\nvY+.");
	im_add_keymap_key(im, KEY_DOWN | RELEASE_KEY, "vY0.");
	im_add_keymap_key(im, KEY_LEFT, "vX-.");
	im_add_keymap_key(im, KEY_LEFT | RELEASE_KEY, "vX0.");
	im_add_keymap_key(im, KEY_RIGHT, "vX+.");
	im_add_keymap_key(im, KEY_RIGHT | RELEASE_KEY, "vX0.");
	im_add_keymap_key(im, KEY_ENTER, "vMl1.");
	im_add_keymap_key(im, KEY_ENTER | RELEASE_KEY, "vMl0.");
	im_add_keymap_key(im, KEY_MENU, "/o.");

	/* Extra shortcuts for non-phone environments */
	im_add_keymap_key(im, KEY_LEFTALT, "+z");
	im_add_keymap_key(im, KEY_LEFTALT | RELEASE_KEY, "-z");
	im_add_keymap_key(im, KEY_SPACE, "ztd-d;\nzt+d;");
	im_add_keymap_key(im, KEY_RIGHTALT, "ztd-d;\nzta-a+u;\nztu-u+n;\nztn-n;\nzt+a;");
	im_add_keymap_key(im, KEY_COMPOSE, "/o.");
}

static void im_commit(struct bsh_input_method *im)
{
	BshShell *shell = bsh_shell_get_default();

	g_debug("im_commit");
	zwp_input_method_v2_commit(im->input_method, im->serial);
	wl_display_flush(shell->disp);
}

static void im_commit_text(struct bsh_input_method *im, const char *text)
{
	g_debug("commit_text %s", text);
	zwp_input_method_v2_commit_string(im->input_method, text);
}

static void im_commit_pending(struct bsh_input_method *im)
{
	char commit[6] = {};
	if(!im->current_char) return;
	g_unichar_to_utf8(g_utf8_get_char(im->current_char), commit);
	im_commit_text(im, commit);
	im->next_state &= ~IM_FLAG_PENDING_AFFECTS_CASE;
	im->current_char = im->current_chars = NULL;
}

static void im_remove_preedit(struct bsh_input_method *im)
{
	g_debug("remove_preedit");
	zwp_input_method_v2_set_preedit_string(im->input_method, "", 0, 0);
}

static void im_update_preedit(struct bsh_input_method *im, const char *chars)
{
	gunichar uch;
	char preedit[6] = {};
	if(im->current_chars == chars){
		im->current_char = g_utf8_next_char(im->current_char);
		if(*im->current_char == '\n' || !*im->current_char){
			im->current_char = im->current_chars;
		}
	}
	else {
		im_commit_pending(im);
		im->current_state = im->next_state;
		im->current_char = im->current_chars = chars;
	}
	g_debug("update_preedit %s", im->current_char);
	uch = g_utf8_get_char(im->current_char);
	g_unichar_to_utf8(uch, preedit);
	zwp_input_method_v2_set_preedit_string(im->input_method, preedit,
			strlen(preedit), strlen(preedit));

	/* override value set in handle_surrounding_text if required by our rules */
	if(want_autolowercase(uch)){
		g_debug("autolower due to pending");
		im->next_state |= IM_FLAG_PENDING_AFFECTS_CASE;
		im->next_state &= ~IM_FLAG_CAPITALIZE;
	}
	else if(want_autouppercase(uch)){
		g_debug("autoupper due to pending");
		im->next_state |= IM_FLAG_PENDING_AFFECTS_CASE;
		im->next_state |= IM_FLAG_CAPITALIZE;
	}
	else {
		g_debug("no case preference for pending");
		im->next_state &= ~IM_FLAG_PENDING_AFFECTS_CASE;
	}
}

static gboolean vp_move_handler(gpointer user_data)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_input_method *im = user_data;

	if (!im->virtual_pointer)
		return G_SOURCE_REMOVE;

	zwlr_virtual_pointer_v1_motion(im->virtual_pointer,
				       im->move_timestamp,
				       im->move_x * 1000,
				       im->move_y * 1000);
	zwlr_virtual_pointer_v1_frame(im->virtual_pointer);
	wl_display_flush(shell->disp);
	im->move_timestamp += 50;

	return G_SOURCE_CONTINUE;
}

static void vp_move_reschedule(struct bsh_input_method *im)
{
	if (!im->move_x && !im->move_y && im->move_timeout) {
		g_source_remove(im->move_timeout);
		im->move_timeout = 0;
	} else if (!im->move_timeout && (im->move_x || im->move_y)) {
		im->move_timeout = g_timeout_add(50, vp_move_handler, im);
	}
}

static void vp_move_x(struct bsh_input_method *im, uint32_t time, int d)
{
	im->move_x = d;
	im->move_timestamp = time;
	vp_move_reschedule(im);
}

static void vp_move_y(struct bsh_input_method *im, uint32_t time, int d)
{
	im->move_y = d;
	im->move_timestamp = time;
	vp_move_reschedule(im);
}

static void vp_button(struct bsh_input_method *im, uint32_t time, uint32_t code, int pressed)
{
	BshShell *shell = bsh_shell_get_default();

	if (!im->virtual_pointer)
		return;

	zwlr_virtual_pointer_v1_button(im->virtual_pointer,
				       time, code, pressed
				       ? WL_POINTER_BUTTON_STATE_PRESSED
				       : WL_POINTER_BUTTON_STATE_RELEASED);
	zwlr_virtual_pointer_v1_frame(im->virtual_pointer);
	wl_display_flush(shell->disp);
}

static void vp_enable(struct bsh_input_method *im)
{
	BshWayland *wl = bsh_wayland_get_default();
	if (im->virtual_pointer)
		return;
	im->virtual_pointer = zwlr_virtual_pointer_manager_v1_create_virtual_pointer(
			bsh_wayland_get_zwlr_virtual_pointer_manager_v1(wl),
			bsh_wayland_get_wl_seat(wl));
}

static void vp_disable(struct bsh_input_method *im)
{
	g_clear_pointer(&im->virtual_pointer, zwlr_virtual_pointer_v1_destroy);
}

static int im_handle_key(struct bsh_input_method *im, uint32_t time,
			 uint32_t keycode, bool down)
{
	BshShell *shell = bsh_shell_get_default();
	uint32_t required_flags = 0;
	const char *action = g_hash_table_lookup(
		im->keys, GINT_TO_POINTER(keycode | (down ? 0 : RELEASE_KEY)));

	if (!action)
		return 0;

	/* TODO: move generic rules to configuration */
	if (im->next_state & IM_FLAG_AUTOCAPITALIZE) {
		if (im->next_state & IM_FLAG_CAPITALIZE)
			im->next_state |= IM_FLAG_UPPERCASE;
		else
			im->next_state &= ~IM_FLAG_UPPERCASE;
	}

	/* First loop: find action */
	while (1) {
		/* flag labels: abcdefghijklmnopqrstuvwxyz012345 (32 flags) */
		if (*action >= '0' && *action <= '5')
			required_flags |= 1 << (*action - '0' + 26);
		else if (*action >= 'a' && *action <= 'z')
			required_flags |= 1 << (*action - 'a');
		/* check if the key is in select mode */
		else if ((im->current_state & required_flags) == required_flags &&
			 *action == ':' && im->current_chars == action + 1) {
			action++;
			if (!(im->current_state & IM_FLAG_EDITING)) {
				g_warning("Cannot insert when input inactive");
			}
			im_update_preedit(im, action);
			im_commit(im);
			return 1;
		}
		/* if flags do not match, look for next action */
		else if ((im->next_state & required_flags) != required_flags) {
			while (*action && *action != '\n')
				action++;
			if (!*action)
				return 0;
			required_flags = 0;
		}
		/* if we found a match, start processing */
		else {
			break;
		}
		action++;
	}

	/* Second loop: process action */
	while (*action) {
		/* activate select mode (only valid when editing) (always stops) */
		if (*action == ':') {
			action++;
			if (!(im->next_state & IM_FLAG_EDITING)) {
				g_warning("Cannot insert when input inactive");
			}
			im_update_preedit(im, action);
			im_commit(im);
			return 1;
		}
		/* insert static (only valid when editing) (always stops) */
		if (*action == '_') {
			char *copy, *terminator;
			action++;
			if (!(im->next_state & IM_FLAG_EDITING)) {
				g_warning("Cannot insert when input inactive");
			}
			im_remove_preedit(im);
			im_commit_pending(im);
			copy = g_strdup(action);
			terminator = strchr(copy, '\n');
			if (terminator)
				*terminator = '\0';
			im_commit_text(im, copy);
			g_free(copy);
			im_commit(im);
			return 1;
		}
		/* stop and forward key */
		if (*action == ';') {
			return 0;
		}
		/* stop and do not forward key */
		if (*action == '.') {
			return 1;
		}
		/* commit pending */
		if (*action == ',') {
			im_commit_pending(im);
		}
		/* set flag */
		else if (*action == '+') {
			action++;
			if (*action >= '0' && *action <= '5')
				im->next_state |= 1 << (*action - '0' + 26);
			else if (*action >= 'a' && *action <= 'z')
				im->next_state |= 1 << (*action - 'a');
			else
				g_warning("Invalid flag in +%c", *action);
		}
		/* unset flag */
		else if (*action == '-') {
			action++;
			if (*action >= '0' && *action <= '5')
				im->next_state &= ~(1 << (*action - '0' + 26));
			else if (*action >= 'a' && *action <= 'z')
				im->next_state &= ~(1 << (*action - 'a'));
			else
				g_warning("Invalid flag in -%c", *action);
		}
		/* toggle flag */
		else if (*action == '^') {
			action++;
			if (*action >= '0' && *action <= '5')
				im->next_state ^= 1 << (*action - '0' + 26);
			else if (*action >= 'a' && *action <= 'z')
				im->next_state ^= 1 << (*action - 'a');
			else
				g_warning("Invalid flag in ^%c", *action);
		} else if (*action == '/') {
			action++;
			if (*action == 'o')
				homescreen_show_overview(shell->homescreen);
			else if (*action == 'h')
				homescreen_show(shell->homescreen);
			else if (*action == 's')
				settings_show(shell->settings);
			else
				g_warning("Invalid operator /%c", *action);
		} else if (*action == 'X') {
			action++;
			if (*action == '-')
				vp_move_x(im, time, -1);
			else if (*action == '+')
				vp_move_x(im, time, 1);
			else if (*action == '0')
				vp_move_x(im, time, 0);
			else
				g_warning("Invalid operator Y%c", *action);
		} else if (*action == 'Y') {
			action++;
			if (*action == '-')
				vp_move_y(im, time, -1);
			else if (*action == '+')
				vp_move_y(im, time, 1);
			else if (*action == '0')
				vp_move_y(im, time, 0);
			else
				g_warning("Invalid operator Y%c", *action);
		} else if (*action == 'M') {
			action++;
			if (action[0] == 'l' && action[1] == '0')
				vp_button(im, time, BTN_LEFT, 0);
			else if (action[0] == 'l' && action[1] == '1')
				vp_button(im, time, BTN_LEFT, 1);
			else if (action[0] == 'r' && action[1] == '0')
				vp_button(im, time, BTN_RIGHT, 0);
			else if (action[0] == 'r' && action[1] == '1')
				vp_button(im, time, BTN_RIGHT, 1);
			else
				g_warning("Invalid operator M%c%c",
				          action[0], action[1]);
			action++;
		} else {
			g_warning("Invalid flag or operator %c", *action);
		}
		action++;
	}
	return 0;
}

static const char *get_mode_string(struct bsh_input_method *im)
{
	if (im->next_state & IM_FLAG_DISABLED)
		return "--";
	if (im->next_state & IM_FLAG_AUTOCAPITALIZE) {
		if (im->next_state & IM_FLAG_CAPITALIZE)
			return "Ab^";
		return "Ab";
	} else {
		if (im->next_state & IM_FLAG_UPPERCASE)
			return "AB";
		if (im->next_state & IM_FLAG_NUMERIC)
			return "12";
	}
	return "ab";
}

static void keyboard_handle_keymap(void *data,
		struct zwp_input_method_keyboard_grab_v2 *grab,
		uint32_t format, int32_t fd, uint32_t size)
{
	struct bsh_input_method *im = data;
	zwp_virtual_keyboard_v1_keymap(im->virtual_keyboard, format, fd, size);
}

static void keyboard_handle_key(void *data,
		struct zwp_input_method_keyboard_grab_v2 *grab,
		uint32_t serial, uint32_t time, uint32_t key, uint32_t down)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_input_method *im = data;
	uint32_t old_flags = im->next_state;
	bool ret = im_handle_key(im, time, key, down);

	if(old_flags != im->next_state){
		g_debug("key updated state");
		if(im->next_state & IM_FLAG_VIRTUAL_POINTER)
			vp_enable(im);
		else
			vp_disable(im);
		if(im->next_state & IM_FLAG_EDITING){
			set_top_panel_input_method_status(shell->top_panel,
					get_mode_string(im));
		}
	}
	if(ret) return;

	/* ensure there is no preedit text when passing unhandled keydown */
	if(down && im->current_char){
		im_remove_preedit(im);
		im_commit_pending(im);
		g_debug("autocommit due to unhandled key");
		zwp_input_method_v2_commit(im->input_method, im->serial);
	}

	zwp_virtual_keyboard_v1_key(im->virtual_keyboard, time, key, down);
	/* Key events should arrive immediately */
	wl_display_flush(shell->disp);
}

static void keyboard_handle_modifiers(void *data,
		struct zwp_input_method_keyboard_grab_v2 *grab,
		uint32_t serial, uint32_t depressed,
		uint32_t latched, uint32_t locked, uint32_t group)
{
	struct bsh_input_method *im = data;
	zwp_virtual_keyboard_v1_modifiers(im->virtual_keyboard,
			depressed, latched, locked, group);
}

static void keyboard_handle_repeat_info(void *data,
		struct zwp_input_method_keyboard_grab_v2 *grab,
		int32_t rate, int32_t delay)
{
}

static const struct zwp_input_method_keyboard_grab_v2_listener keyboard_grab_listener = {
	.keymap = keyboard_handle_keymap,
	.key = keyboard_handle_key,
	.modifiers = keyboard_handle_modifiers,
	.repeat_info = keyboard_handle_repeat_info,
};

static void handle_activate(void *data, struct zwp_input_method_v2 *input_method)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_input_method *im = data;

	g_debug("activate");
	im->next_state = IM_FLAG_EDITING | IM_FLAG_AUTOCAPITALIZE;
	set_top_panel_input_method_status(shell->top_panel, get_mode_string(im));
}

static void handle_deactivate(void *data, struct zwp_input_method_v2 *input_method)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_input_method *im = data;

	g_debug("deactivate");
	im->current_state = im->next_state = 0;
	im->current_char = im->current_chars = NULL;
	set_top_panel_input_method_status(shell->top_panel, NULL);
}

static void handle_surrounding_text(void *data,
		struct zwp_input_method_v2 *input_method, const char *text,
		uint32_t cursor, uint32_t anchor)
{
	BshShell *shell = bsh_shell_get_default();
	struct bsh_input_method *im = data;

	if(!(im->next_state & IM_FLAG_EDITING))
		return;

	if(!g_utf8_validate(text, 0, NULL)){
		g_warning("Invalid UTF8 surrounding text");
		return;
	}
	if(cursor > strlen(text)){
		g_warning("Invalid cursor");
		return;
	}
	if(!(im->next_state & IM_FLAG_PENDING_AFFECTS_CASE))
	{
		if(cursor == 0){
			g_debug("autoupper forced by cursor==0");
			im->next_state |= IM_FLAG_CAPITALIZE;
		}
		else {
			gunichar uch;
			const char *ch = text + cursor - 1;
			uch = g_utf8_get_char(ch);
			while(ch != text &&
				!want_autolowercase(uch) &&
				!want_autouppercase(uch))
			{
				ch = g_utf8_prev_char(ch);
				uch = g_utf8_get_char(ch);
			}
			if(want_autolowercase(uch)){
				g_debug("autolower due to surrounding text");
				im->next_state &= ~IM_FLAG_CAPITALIZE;
			}
			else {
				g_debug("autoupper due to surrounding text");
				im->next_state |= IM_FLAG_CAPITALIZE;
			}
		}
		set_top_panel_input_method_status(shell->top_panel,
				get_mode_string(im));
	}
}

static void handle_text_change_cause(void *data,
		struct zwp_input_method_v2 *input_method, uint32_t cause)
{
}

static void handle_content_type(void *data,
		struct zwp_input_method_v2 *input_method,
		uint32_t hint, uint32_t purpose)
{
	g_debug("Content type: 0x%03x %u", hint, purpose);
}

static void handle_done(void *data, struct zwp_input_method_v2 *input_method)
{
	struct bsh_input_method *im = data;
	im->serial++;
}

static void handle_unavailable(void *data, struct zwp_input_method_v2 *input_method)
{
	g_warning("Could not register as input method");
}

static const struct zwp_input_method_v2_listener input_method_listener = {
	.activate = handle_activate,
	.deactivate = handle_deactivate,
	.surrounding_text = handle_surrounding_text,
	.text_change_cause = handle_text_change_cause,
	.content_type = handle_content_type,
	.done = handle_done,
	.unavailable = handle_unavailable
};

struct bsh_input_method *create_input_method(void)
{
	BshWayland *wl = bsh_wayland_get_default();
	struct bsh_input_method *im = g_new0(struct bsh_input_method, 1);

	im->input_method = zwp_input_method_manager_v2_get_input_method(
			bsh_wayland_get_zwp_input_method_manager_v2(wl),
			bsh_wayland_get_wl_seat(wl));
	zwp_input_method_v2_add_listener(im->input_method,
			&input_method_listener, im);
	im->grab = zwp_input_method_v2_grab_keyboard(im->input_method);
	zwp_input_method_keyboard_grab_v2_add_listener(im->grab,
			&keyboard_grab_listener, im);
	im->virtual_keyboard = zwp_virtual_keyboard_manager_v1_create_virtual_keyboard(
			bsh_wayland_get_zwp_virtual_keyboard_manager_v1(wl),
			bsh_wayland_get_wl_seat(wl));
	im_load_keymap(im);
	return im;
}

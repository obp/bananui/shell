/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <bananui/keys.h>

#include <glib-object.h>

G_BEGIN_DECLS

#define BSH_TYPE_DIALER bsh_dialer_get_type()

G_DECLARE_FINAL_TYPE (BshDialer, bsh_dialer, BSH, DIALER, GObject)

struct wl_output;

BshDialer *bsh_dialer_new (struct wl_output *wl_output);
void       bsh_dialer_show (BshDialer *self);
void       bsh_dialer_hide (BshDialer *self);
void       bsh_dialer_key (BshDialer *self, xkb_keysym_t key);

G_END_DECLS

/*
 * Based on src/phosh-wayland.h from phosh
 *
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "enums.h"

#include "ext-idle-notify-v1-client-protocol.h"
#include "input-method-unstable-v2-client-protocol.h"
#include "virtual-keyboard-unstable-v1-client-protocol.h"
#include "wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"
#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "wlr-gamma-control-unstable-v1-client-protocol.h"
#include "wlr-output-management-unstable-v1-client-protocol.h"
#include "wlr-output-power-management-unstable-v1-client-protocol.h"
#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "wlr-virtual-pointer-unstable-v1-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"
#include "xdg-shell-client-protocol.h"
#include "phoc-layer-shell-effects-unstable-v1-client-protocol.h"

/* This goes past the other wl protocols since it might need their structs */
#include "phosh-private-client-protocol.h"

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * BshWaylandSeatCapabilities:
 * @BSH_WAYLAND_SEAT_CAPABILITY_NONE: no device detected
 * @BSH_WAYLAND_SEAT_CAPABILITY_POINTER: the seat has pointer devices
 * @BSH_WAYLAND_SEAT_CAPABILITY_KEYBOARD: the seat has one or more keyboards
 * @BSH_WAYLAND_SEAT_CAPABILITY_TOUCH: the seat has touch devices
 *
 * These match wl_seat_capabilities
 */
typedef enum {
  BSH_WAYLAND_SEAT_CAPABILITY_NONE     = 0,
  BSH_WAYLAND_SEAT_CAPABILITY_POINTER  = (1 << 0),
  BSH_WAYLAND_SEAT_CAPABILITY_KEYBOARD = (1 << 1),
  BSH_WAYLAND_SEAT_CAPABILITY_TOUCH    = (1 << 2),
} BshWaylandSeatCapabilities;

/* Versions of phosh-private protocol that add certain features */
#define BSH_PRIVATE_GET_THUMBNAIL_SINCE  4
#define BSH_PRIVATE_KBD_EVENTS_SINCE     5
#define BSH_PRIVATE_STARTUP_NOTIFY_SINCE 6
#define BSH_PRIVATE_SHELL_READY_SINCE    6

#define BSH_TYPE_WAYLAND bsh_wayland_get_type()

G_DECLARE_FINAL_TYPE (BshWayland, bsh_wayland, BSH, WAYLAND, GObject)

BshWayland                           *bsh_wayland_get_default (void);
GHashTable                           *bsh_wayland_get_wl_outputs (BshWayland *self);
gboolean                              bsh_wayland_has_wl_output  (BshWayland *self,
                                                                    struct wl_output *wl_output);
struct ext_idle_notifier_v1          *bsh_wayland_get_ext_idle_notifier_v1 (BshWayland *self);
struct phosh_private                 *bsh_wayland_get_phosh_private (BshWayland *self);
uint32_t                              bsh_wayland_get_phosh_private_version (BshWayland *self);
struct wl_seat                       *bsh_wayland_get_wl_seat (BshWayland *self);
struct wl_shm                        *bsh_wayland_get_wl_shm (BshWayland *self);
struct xdg_wm_base                   *bsh_wayland_get_xdg_wm_base (BshWayland *self);
struct zwlr_foreign_toplevel_manager_v1 *bsh_wayland_get_zwlr_foreign_toplevel_manager_v1 (BshWayland *self);
struct zwlr_layer_shell_v1           *bsh_wayland_get_zwlr_layer_shell_v1 (BshWayland *self);
struct zwlr_gamma_control_manager_v1 *bsh_wayland_get_zwlr_gamma_control_manager_v1 (BshWayland *self);
struct zwlr_output_manager_v1        *bsh_wayland_get_zwlr_output_manager_v1 (BshWayland *self);
struct zwlr_output_power_manager_v1  *bsh_wayland_get_zwlr_output_power_manager_v1 (BshWayland *self);
struct zxdg_output_manager_v1        *bsh_wayland_get_zxdg_output_manager_v1 (BshWayland *self);
struct zwlr_screencopy_manager_v1    *bsh_wayland_get_zwlr_screencopy_manager_v1 (BshWayland *self);
struct zwp_input_method_manager_v2   *bsh_wayland_get_zwp_input_method_manager_v2 (BshWayland *self);
struct zwp_virtual_keyboard_manager_v1 *bsh_wayland_get_zwp_virtual_keyboard_manager_v1 (BshWayland *self);
struct zwlr_virtual_pointer_manager_v1 *bsh_wayland_get_zwlr_virtual_pointer_manager_v1 (BshWayland *self);
void                                  bsh_wayland_roundtrip (BshWayland *self);
BshWaylandSeatCapabilities            bsh_wayland_get_seat_capabilities (BshWayland *self);
struct zphoc_layer_shell_effects_v1  *bsh_wayland_get_zphoc_layer_shell_effects_v1 (BshWayland *self);

G_END_DECLS

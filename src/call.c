/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "bsh-call"

#define LIBFEEDBACK_USE_UNSTABLE_API

#include "call.h"

#include <bananui/bananui.h>
#include <libfeedback.h>
#include "call_counter.h"
#include "shell.h"
#include "slot_dialog.h"
#include "layerwindow.h"

enum {
  BSH_CALL_PROP_0,
  BSH_CALL_PROP_CALL,
  BSH_CALL_PROP_LAST_PROP,
};
static GParamSpec *props[BSH_CALL_PROP_LAST_PROP];

struct _BshCall {
  GObject parent;

  WroomdDBusWroomdCall *call;
  bWindow              *wnd;
  bCard                *card;
  bContent             *type, *number, *state;
  LfbEvent             *fb;
};

G_DEFINE_TYPE (BshCall, bsh_call, G_TYPE_OBJECT)


static void
bsh_call_get_property (GObject *object,
                       guint property_id,
                       GValue *value,
                       GParamSpec *pspec)
{
  BshCall *self = BSH_CALL (object);

  switch (property_id) {
  case BSH_CALL_PROP_CALL:
    g_value_set_object (value, self->call);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
properties_changed_cb (WroomdDBusWroomdCall *call,
                       GVariant             *changed,
                       GStrv                 invalidated,
                       BshCall              *self)
{
  const char *state = wroomd_dbus_wroomd_call_get_state (self->call);
  const char *direction = wroomd_dbus_wroomd_call_get_direction (self->call);

  if (self->number)
    bSetText (self->number, wroomd_dbus_wroomd_call_get_number (self->call));
  if (self->state)
    bSetText (self->state, state);

  if (0 == strcmp (state, "ringing") && 0 == strcmp (direction, "incoming") &&
      !self->fb) {
    self->fb = lfb_event_new ("phone-incoming-call");
    lfb_event_set_timeout (self->fb, 0);
    lfb_event_trigger_feedback_async (self->fb, NULL, NULL, NULL);
  } else if (self->fb) {
    lfb_event_end_feedback_async (self->fb, NULL, NULL, NULL);
    g_object_unref (self->fb);
  }

  bRequestFrame (self->wnd);
}


static void
bsh_call_set_property (GObject *object,
                       guint property_id,
                       const GValue *value,
                       GParamSpec *pspec)
{
  BshCall *self = BSH_CALL (object);

  switch (property_id) {
  case BSH_CALL_PROP_CALL:
    g_set_object (&self->call, g_value_get_object (value));
    g_signal_connect_object (self->call,
                             "g-properties-changed",
                             G_CALLBACK(properties_changed_cb),
                             self,
                             G_CONNECT_DEFAULT);
    properties_changed_cb (self->call, NULL, NULL, self);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
bsh_call_dispose (GObject *object)
{
  BshCall *self = BSH_CALL (object);

  callcounter_decrease ();
  g_clear_pointer (&self->wnd, bDestroyWindow);
  g_clear_pointer (&self->card, bDestroyCard);

  G_OBJECT_CLASS (bsh_call_parent_class)->dispose (object);
}


static void
bsh_call_class_init (BshCallClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bsh_call_dispose;

  object_class->get_property = bsh_call_get_property;
  object_class->set_property = bsh_call_set_property;

  props[BSH_CALL_PROP_CALL] =
    g_param_spec_object ("call",
                         "Call",
                         "The D-Bus call object",
			 WROOMD_DBUS_TYPE_WROOMD_CALL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, BSH_CALL_PROP_LAST_PROP, props);
}


static void
call_accept_cb (WroomdDBusWroomdCall *call,
                GAsyncResult         *res,
                BshCall              *self)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_call_call_accept_finish (call, res, &error)) {
    g_warning ("Error accepting call: %s", error->message);
  } else {
    g_debug ("Successfully accepted call");
  }
}


static void
call_hangup_cb (WroomdDBusWroomdCall *call,
                GAsyncResult         *res,
                BshCall              *self)
{
  g_autoptr(GError) error = NULL;
  if (!wroomd_dbus_wroomd_call_call_hangup_finish (call, res, &error)) {
    g_warning ("Error ending call: %s", error->message);
  } else {
    g_debug ("Successfully ended call");
  }
}


static int
call_handle_key (void *param, void *data)
{
  BshCall *self = data;
  bKeyEvent *ev = param;

  if (0 == strcmp (wroomd_dbus_wroomd_call_get_state (self->call), "ringing") &&
      0 == strcmp (wroomd_dbus_wroomd_call_get_direction (self->call), "incoming")) {
    if (ev->sym == BANANUI_KEY_Call)
      bsh_call_accept (self);
  }

  if (ev->sym == BANANUI_KEY_EndCall || ev->sym == BANANUI_KEY_Back)
    bsh_call_hangup (self);

  return 0;
}


static int
call_render (void *param, void *data)
{
  BshCall *self = data;

  bRenderCard (self->wnd, self->card);

  return 1;
}


static void
bsh_call_init (BshCall *self)
{
  bBox *boxes[3];
  self->card = bBuildCard (bDefaultTheme (NULL),
                           "shell call",
                           3, boxes);
  if (boxes[0])
    bAddContent (boxes[0], self->type = bCreateText("Voice call"));
  if (boxes[1])
    bAddContent (boxes[1], self->number = bCreateText(""));
  if (boxes[2])
    bAddContent (boxes[2], self->state = bCreateText(""));

  self->wnd = bCreateLayerWindow ("bananui-shell call",
                                  NULL, 0, 0, /* FIXME: get output from shell */
                                  ZWLR_LAYER_SHELL_V1_LAYER_TOP,
                                  -30, 0, 0, 0,
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
                                  0, 1);
  if (!self->wnd) {
    g_critical ("Failed to show call");
  } else {
    bRegisterEventHandler (&self->wnd->keydown, call_handle_key, self);
    bRegisterEventHandler (&self->wnd->redraw, call_render, self);
    bRequestFrame (self->wnd);
  }

  callcounter_increase ();
  bsh_shell_wake (bsh_shell_get_default ());
}


void
bsh_call_accept (BshCall *self)
{
  wroomd_dbus_wroomd_call_call_accept (
    self->call,
    NULL,
    (GAsyncReadyCallback) call_accept_cb,
    self);
}


void
bsh_call_hangup (BshCall *self)
{
  wroomd_dbus_wroomd_call_call_hangup (
    self->call,
    NULL,
    (GAsyncReadyCallback) call_hangup_cb,
    self);
}


BshCall *
bsh_call_new (WroomdDBusWroomdCall *call)
{
  return g_object_new (BSH_TYPE_CALL,
                       "call", call,
                       NULL);
}


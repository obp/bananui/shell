/*
 * This file is part of bananui-shell.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bsh-top-panel"

#define GNOME_DESKTOP_USE_UNSTABLE_API
#include <libgnome-desktop/gnome-wall-clock.h>
#include <bananui/bananui.h>
#include "top_panel.h"
#include "layerwindow.h"

struct bsh_top_panel {
	bWindow *wnd;
	bCard *card;
	GnomeWallClock *wall_clock;
	bContent *clock, *im_icon, *im_status;
};

static int top_panel_render(void *param, void *data)
{
	struct bsh_top_panel *p = data;
	bRenderCard(p->wnd, p->card);
	return 1;
}

void set_top_panel_color(struct bsh_top_panel *p,
		double r, double g, double b, double a,
		double cr, double cg, double cb, double ca)
{
	p->card->style.override_background_color = 1;
	p->card->style.items.background_color[0] = r;
	p->card->style.items.background_color[1] = g;
	p->card->style.items.background_color[2] = b;
	p->card->style.items.background_color[3] = a;
	p->card->style.override_content_color = 1;
	p->card->style.items.content_color[0] = cr;
	p->card->style.items.content_color[1] = cg;
	p->card->style.items.content_color[2] = cb;
	p->card->style.items.content_color[3] = ca;
}

void unset_top_panel_color(struct bsh_top_panel *p)
{
	p->card->style.override_background_color = 0;
	p->card->style.override_content_color = 0;
}

void set_top_panel_input_method_status(struct bsh_top_panel *p, const char *status)
{
	if (status) {
		if (p->im_icon)
			bSetIcon(p->im_icon, "document-edit-symbolic", 16);
		if (p->im_status)
			bSetText(p->im_status, status);
	} else {
		if (p->im_icon)
			bClearContent(p->im_icon);
		if (p->im_status)
			bClearContent(p->im_status);
	}
	bInvalidateWithChildren(p->card->box);
	bRequestFrame(p->wnd);
}

static void handle_clock_update(GnomeWallClock *clock,
		GParamSpec *pspec, struct bsh_top_panel *p)
{
	if (p->clock)
		bSetText(p->clock, gnome_wall_clock_get_clock(clock));
	bInvalidateWithChildren(p->card->box);
	bRequestFrame(p->wnd);
}

struct bsh_top_panel *create_top_panel(struct wl_output *output, int height)
{
	bBox *boxes[3] = { 0 };
	struct bsh_top_panel *p = g_malloc0(sizeof(struct bsh_top_panel));

	p->wnd = bCreateLayerWindow("bananui-shell top panel",
			output, 0, height, ZWLR_LAYER_SHELL_V1_LAYER_TOP,
			0, 0, 0, 0,
			ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
			ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
			ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT,
			height, 0);
	if(!p->wnd){
		g_free(p);
		return NULL;
	}

	p->wall_clock = gnome_wall_clock_new();
	g_object_set(p->wall_clock, "time-only", TRUE, NULL);
	g_signal_connect(p->wall_clock, "notify::clock",
			G_CALLBACK(handle_clock_update), p);

	p->card = bBuildCard(bDefaultTheme(NULL),
			     "shell top panel",
			     3,
			     boxes);
	if (!p->card)
		g_error("Failed to build top panel card");
	if (boxes[0])
		bAddContent(boxes[0], p->im_icon = bCreateEmpty());
	if (boxes[1])
		bAddContent(boxes[1], p->im_status = bCreateEmpty());
	if (boxes[2])
		bAddContent(boxes[2], p->clock = bCreateText(
				    gnome_wall_clock_get_clock(p->wall_clock)));

	bRegisterEventHandler(&p->wnd->redraw, top_panel_render, p);
	bRequestFrame(p->wnd);
	return p;
}

void destroy_top_panel(struct bsh_top_panel *p)
{
	g_object_unref(p->wall_clock);
	bDestroyWindow(p->wnd);
	bDestroyCard(p->card);
	g_free(p);
}

Bananui's user-facing shell, to be used with [phoc](https://gitlab.gnome.org/World/Phosh/phoc)
and intended as a simple keypad-oriented replacement for phosh.

This repository contains a lot of code taken from phosh, slightly modified
to work with Bananui.

## Building

Use the Meson build system, just like with libbananui:

```
meson setup _build
ninja -C _build
ninja -C _build install
```

## Running

```
bananui-session
```

## Code style
The code is a mess. Some of the newer code uses GObject and the same coding
style as Phosh. Older code is written without any specific style.

## Contributing
Contributions are welcome. For a list of contributors, see <https://git.abscue.de/obp/bananui/shell/-/graphs/main>.
